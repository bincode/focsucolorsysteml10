// Import modules
import initialize from '../table-inputs/select2-table-initialization.js';
import { productByPlanConfig, existingRackSelectConfig } from '../selects/select2-initialization.js';

// Call the function when the page is ready
$(function () {
    handleCreateFormula();
});

// Call the function when the "plan" select element is changed
$('select[name="plan"]').on('change', function () {
    handleCreateFormula();
});

// Function to handle creating a formula
function handleCreateFormula() {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected value from the "plan" select element
    const id = $('select[name="plan"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/plans/${id}/input`;

    // Send an AJAX GET request to the API endpoint
    $.ajax({
        url: apiUrl,
        method: 'GET',
        success: function (response) {
            // Insert the response HTML into the element with ID "fragmentContainer"
            $('#fragmentContainer').html(response);

            // Initialize select2 inputs on the "table-input-material" table using the provided configuration objects
            initialize('table-input-material', 'productByPlan[]', 'existingRack[]', productByPlanConfig, existingRackSelectConfig);
        },
        error: function () {
            // Error handling if the AJAX request fails
        }
    });
}
