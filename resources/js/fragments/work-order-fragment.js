import initialize from '../table-inputs/select2-table-initialization.js';
import Swal from 'sweetalert2';

import {
    productsSelectConfig,
    racksSelectConfig
} from '../selects/select2-initialization.js';

// Call the function when the page is ready
$(function () {
    handleCreateFormula();
});

// Call the function when the element with ID "create-formula" is clicked
$('#create-formula').on('click', function () {
    handleCreateFormula();
});

function handleCreateFormula() {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected value from the "version" select element and the value from the "quantity" input element
    const id = $('select[name="version"]').val();
    const quantity = $('input[name="quantity"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/formulas/${id}/input?quantity=${quantity}`;

    // Send an AJAX GET request to the API endpoint
    $.ajax({
        url: apiUrl,
        method: 'GET',
        success: function (response) {
            // Insert the response HTML into the element with ID "fragmentContainer"
            $('#fragmentContainer').html(response);

            // Initialize select2 inputs on the "table-input-products" table using the provided configuration objects
            initialize('table-input-products', 'products[]', 'racks[]', productsSelectConfig, racksSelectConfig);

            // Event handler for the change event on the "plan_quantity[]" input elements
            $('input[name="percentage[]"]').on('change', function () {

                var totalPercentage = 0.00;

                // Calculate and update the percentage for each row
                $('input[name="plan_quantity[]"]').each(function () {
                    var totalQuantity = $('input[name="quantity"]').val();

                    var $row = $(this).closest('tr');
                    var percentage = parseFloat($row.find('input[name="percentage[]"]').val());
                    var quantity = isNaN(percentage) ? '' : (totalQuantity * percentage) / 100;

                    if (!isNaN(percentage)) {
                        totalPercentage += percentage;
                    }

                    // Update the "percentage[]" input value with the calculated percentage
                    if (!isNaN(quantity) && quantity !== '') {
                        $(this).val(quantity.toFixed(2));
                    } else {
                        $(this).val('');
                    }

                    if (!isNaN(totalPercentage) && totalPercentage !== '' && totalPercentage == 100) {
                        $('#submitworkorder').removeAttr('disabled').addClass("btn-primary").removeClass("btn-secondary");
                    } else {
                        $('#submitworkorder').attr('disabled', true).addClass('btn-secondary').removeClass('btn-primary');
                    }
                });
            });
        },
        error: function () {
            // Error handling if the AJAX request fails
        }
    });
}


$('#work-order').on('submit', function (e) {
    e.preventDefault();

    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Extract the ID from the current URL
    const id = $('select[name="version"]').val();
    console.log($(this).serialize());

    // Construct the API endpoint URL
    const apiUrl = `${domain}/api/formulas/${id}/validation`;

    $.ajax({
        url: apiUrl,
        method: $(this).attr('method'),
        data: $(this).serialize(),
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                $('#work-order').off('submit').trigger('submit');
            } else {
                Swal.fire({
                    title: 'Confirmation',
                    text: 'The entered formula has changes or additions. Do you want to create a new formula?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#work-order').off('submit').trigger('submit');
                    }
                });
            }
        },
        error: function (xhr, status, error) {
            // Error dalam permintaan Ajax
            Swal.fire({
                title: 'Error',
                text: error,
                icon: 'error'
            });
        }
    });
});
