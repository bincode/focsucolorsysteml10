import '../tables/base-table';
import { reInitiateSelect2 } from '../selects/select2-initialization';

/**
 * Initialize the table with Select2 on the specified select elements.
 */
export default function initialize(tableId, productSelectName, locationSelectName, productSelectConfig, locationSelectConfig) {
    // Get the product and location select elements
    const $product = $(`select[name="${productSelectName}"]`);
    const $location = $(`select[name="${locationSelectName}"]`);

    // Initialize Select2 on the initial select elements
    initializeRow($product, $location, productSelectConfig, locationSelectConfig);

    // Create a DataTable instance for the table
    const table = $(`#${tableId}`).DataTable({
        paging: false,
        order: false,
        info: false,
        lengthChange: false,
        searching: false
    });

    // Event listener for change event on table cells
    $(`#${tableId} tbody`).on('change', 'td', function (e) {

        // Get the row associated with the changed cell
        const row = table.row(this);

        handleTableCellChange(e, table, row, productSelectName, locationSelectName, productSelectConfig, locationSelectConfig);
    });

    // Event listener for click event on delete button
    $(`#${tableId} tbody`).on('click', 'button[id="btn_delete_list"]', function () {
        table.row($(this).parents('tr'))
            .remove()
            .draw();
    });
}

/**
 * Handles the change event on table cells.
 */
function handleTableCellChange(e, table, row, productSelectName, locationSelectName, productSelectConfig, locationSelectConfig) {
    // Get the total number of records in the table
    const totalRecords = table.page.info().recordsTotal;

    // Get the product and location select elements within the row
    const $product = row.nodes().to$().find(`select[name="${productSelectName}"]`);
    const $location = row.nodes().to$().find(`select[name="${locationSelectName}"]`);

    // Get the index of the row
    const rowIndex = row.index();

    // Check if it's the last row in the table
    if (rowIndex === totalRecords - 1) {

        // Get the delete button within the row
        const $button = row.nodes().to$().find('button[id=btn_delete_list]');

        // Enable the delete button and update its classes
        $button.removeAttr('disabled').addClass("btn-danger").removeClass("btn-secondary");

        // Clone the current row
        cloneRow(row, table, productSelectName, locationSelectName, productSelectConfig, locationSelectConfig);
    }

    // Reinitialize Select2 for the current row
    initializeRow($product, $location, productSelectConfig, locationSelectConfig);
}

/**
 * Initializes Select2 for the provided product and location select elements.
 */
function initializeRow($product, $location, productSelectConfig, locationSelectConfig) {

    // Reinitialize Select2 for the provided product and location select elements
    reInitiateSelect2($product, productSelectConfig);
    reInitiateSelect2($location, locationSelectConfig);
}

/**
 * Clones a row in the table.
 */
function cloneRow($row, table, productSelectName, locationSelectName, productSelectConfig, locationSelectConfig) {
    // Clone the current row
    const $clonedRow = $row.node().cloneNode(true);

    // Add the cloned row to the table and redraw it
    const $nextRow = table.row.add($clonedRow).draw();

    // Get the delete button within the row
    const $button = $nextRow.nodes().to$().find('button[id=btn_delete_list]');

    // Disable the delete button and update its classes
    $button.attr('disabled', true).addClass('btn-secondary').removeClass('btn-danger');

    // Remove any previous Select2 elements from the cloned row
    const $nextProduct = $nextRow.nodes().to$().find(`select[name="${productSelectName}"]`);
    const $nextLocation = $nextRow.nodes().to$().find(`select[name="${locationSelectName}"]`);

    $nextProduct.next("span.select2").last().remove();
    $nextLocation.next("span.select2").last().remove();

    // Reinitialize Select2 for the cloned row
    initializeRow($nextProduct, $nextLocation, productSelectConfig, locationSelectConfig);
}
