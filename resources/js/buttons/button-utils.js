/**
 * Render Button Action
 *
 * @param {string} id
 * @param {string} color
 * @param {string} icon
 * @param {boolean} isAllowed
 * @returns string
 */
export function renderButton(id, color, icon, isAllowed, label = '') {
    // Determine the button color based on whether it is allowed or not
    const buttonColor = isAllowed ? color : "btn-secondary";

    // Determine the button class based on whether it is allowed or not
    const buttonClass = isAllowed ? "" : "disabled";

    // Create the tooltip attribute if a label is provided
    const tooltip = label ? `data-toggle="tooltip" data-placement="bottom" title="${label}"` : '';

    // Construct the HTML string for the button element
    return `<button id="${id}" class="btn btn-sm mr-1 ${buttonColor}" ${buttonClass} ${tooltip}><i class="${icon}"></i></button>`;
}
