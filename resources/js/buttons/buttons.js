import { renderButton } from './button-utils.js';

/**
 * Method to create edit action button
 *
 * @param {boolean} isAllowed
 * @returns {string}
 */
export function createEditButton(isAllowed) {
    return renderButton('btn_edit', 'btn-warning', 'fas fa-edit', isAllowed, 'Edit');
}

/**
 * Method to create delete action button
 *
 * @param {boolean} isAllowed
 * @returns {string}
 */
export function createDeleteButton(isAllowed) {
    return renderButton('btn_trash', 'btn-danger', 'fas fa-trash', isAllowed, 'Delete');
}

/**
 * Method to create rack action button
 *
 * @param {boolean} isAllowed
 * @returns {string}
 */
export function createRackButton(isAllowed) {
    return renderButton('btn_rack', 'btn-info', 'fas fa-layer-group', isAllowed, 'See Rack');
}

/**
 * Method to create history action button
 *
 * @param {boolean} isAllowed
 * @returns {string}
 */
export function createHistoryButton(isAllowed) {
    return renderButton('btn_history', 'btn-primary', 'fas fa-history', isAllowed, 'See History');
}
