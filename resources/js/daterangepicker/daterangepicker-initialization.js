import '../moment/moment-initialization';

// Import the daterangepicker library
import 'daterangepicker';

// Initialize datepicker
$('.datepicker').daterangepicker({
    locale: {
        format: 'DD/MM/YYYY'
    },
    autoApply: true,
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 2011,
    maxYear: moment().format('YYYY')
});
