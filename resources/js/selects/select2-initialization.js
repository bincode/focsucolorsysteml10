import '~/admin-lte/plugins/select2/js/select2.full.min.js';

// Global configuration for Select2
const SELECT2_GLOBAL_CONFIG = {
    placeholder: '... Select ...',
    ajax: {
        delay: 250,
    },
};

// Function to transform AJAX response data into Select2-compatible format
function processResults(data) {
    return {
        results: $.map(data.data, function (obj) {
            return {
                id: obj.id,
                text: obj.name,
            };
        }),
    };
}

// Function to configure and initialize Select2
export function configSelect2(selector, config) {
    $(selector).select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Function to configure and initialize Select2
export function reInitiateSelect2($element, config) {
    $element.select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Configuration and initialization for "role" select element
export const roleSelectConfig = {
    placeholder: '.. Select Role ..',
    ajax: {
        url: '/api/roles',
        processResults: processResults,
    },
};
configSelect2("select[name='role']", roleSelectConfig);

// Configuration and initialization for "supplier" select element
export const supplierSelectConfig = {
    placeholder: '.. Select Supplier ..',
    ajax: {
        url: '/api/companies?type=supplier',
        processResults: processResults,
    },
};
configSelect2("select[name='supplier']", supplierSelectConfig);

// Configuration and initialization for "customer" select element
export const customerSelectConfig = {
    placeholder: '.. Select Customer ..',
    ajax: {
        url: '/api/companies?type=customer',
        processResults: processResults,
    },
};
configSelect2("select[name='customer']", customerSelectConfig);

// Configuration and initialization for "products[]" and "product" select element
export const productsSelectConfig = {
    placeholder: '.. Select Product ..',
    ajax: {
        url: '/api/products',
        processResults: processResults,
    },
};
configSelect2("select[name='products[]']", productsSelectConfig);
configSelect2("select[name='product']", productsSelectConfig);

// Configuration and initialization for "racks[]" and "rack" select elements
export const racksSelectConfig = {
    placeholder: '.. Select Rack ..',
    ajax: {
        url: '/api/racks',
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.code,
                    };
                }),
            };
        },
    },
};
configSelect2("select[name='racks[]']", racksSelectConfig);

// Configuration and initialization for "materials[]" select element
export const materialsSelectConfig = {
    placeholder: '.. Select Product ..',
    ajax: {
        url: '/api/products',
        data: function (params) {
            return {
                search: params.term,
                tag: 'RM',
            };
        },
        processResults: processResults,
    },
};
configSelect2("select[name='materials[]']", materialsSelectConfig);

// Configuration and initialization for "goods[]" and "goods" select element
export const goodsSelectConfig = {
    placeholder: '.. Select Product ..',
    ajax: {
        url: '/api/products?tag=FG',
        processResults: processResults,
    },
};
configSelect2("select[name='goods[]']", goodsSelectConfig);
configSelect2("select[name='goods']", goodsSelectConfig);

// Configuration and initialization for "version" select element
export const formulaVersionSelectConfig = {
    placeholder: '.. Select Product ..',
    ajax: {
        url: '/api/formulas',
        data: function (params) {
            return {
                product: $('select[name="goods"]').val(),
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.version,
                    };
                }),
            };
        },
    },
};
configSelect2("select[name='version']", formulaVersionSelectConfig);

$('select[name="goods"]').on('change', function () {
    configSelect2("select[name='version']", formulaVersionSelectConfig);
});

// Configuration and initialization for "plan" select element
export const productionPlanSelectConfig = {
    placeholder: '.. Select No. Lot ..',
    ajax: {
        url: '/api/plans',
        data: function (params) {
            return {
                search: params.term,
                status: 'Planning Phase',
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.lot,
                        product: obj.product,
                        version: obj.version,
                        quantity: obj.quantity,
                    };
                }),
            };
        },
    },
};
configSelect2("select[name='plan']", productionPlanSelectConfig);

$('select[name="plan"]').on('select2:select', function (evt) {
    $('input[name="productName"]').val(evt.params.data.product);
    $('input[name="version"]').val(evt.params.data.version);
    $('input[name="quantity_plan"]').val(evt.params.data.quantity);

    configSelect2("select[name='productByPlanConfig[]']", productByPlanConfig);
});

// Configuration and initialization for "existingRacks[]" and "rack" select elements
export const existingRackSelectConfig = {
    placeholder: '.. Select Rack ..',
    ajax: {
        url: '/api/racks',
        data: function (params) {
            return {
                search: params.term,
                product: $(this).closest('tr').find('select[name="productByPlan[]"]').val(),
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.code,
                    };
                }),
            };
        },
    },
};
configSelect2("select[name='existingRack[]']", existingRackSelectConfig);

// Configuration and initialization for "productByPlan" select element
export const productByPlanConfig = {
    placeholder: '... Select ...',
    ajax: {
        delay: 250,
        url: '/api/products',
        data: function (params) {
            return {
                search: params.term,
                plan: $('select[name="plan"]').val(),
            };
        },
        processResults: processResults,
    },
};
configSelect2("select[name='productByPlan[]']", productByPlanConfig);
