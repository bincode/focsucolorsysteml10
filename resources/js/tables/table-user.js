import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
        title: 'Username',
        data: 'username',
    },
    {
        title: 'Surname',
        data: 'surname'
    },
    {
        title: 'Role',
        data: 'role'
    },
    {
        title: 'Actions',
        data: null,
        width: '5%',
        class: 'text-center',
        searchable: false,
        render: function (data, type, row) {
            // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
            const canEditOrDelete = data.id !== 1;
            const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(canEditOrDelete);

            // Return the HTML for the Edit and Delete buttons
            return editAndDeleteButtonsHTML;
        }
    }
];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbUser', '/api/users', columns);
