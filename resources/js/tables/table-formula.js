import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
        title: 'Product Name',
        data: 'name',
        width: '50%'
    },
    {
        title: 'Version',
        data: 'version',
    },
    {
        title: 'Actions',
        data: null,
        width: '5%',
        class: 'text-center',
        searchable: false,
        render: function (data, type, row) {
            // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
            const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true);

            // Return the HTML for the Edit and Delete buttons
            return editAndDeleteButtonsHTML;
        }
    }
];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbFormula', '/api/formulas', columns);
