import { createBaseTable } from './base-table';

let quantity = 0;

// Define the column configurations for the DataTable
const columns = [{
        title: 'Date',
        data: 'date',
        width: '10%',
    },
    {
        title: 'Reference',
        data: 'reference',
    },
    {
        title: 'Description',
        data: 'description',
    },
    {
        title: 'Location',
        data: 'location',
    },
    {
        title: 'In',
        data: 'in',
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            // Calculate the total quantity
            quantity += data;

            // Format and return the formatted output
            return Number(data.toFixed(3));
        },
    },
    {
        title: 'Out',
        data: 'out',
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            // Calculate the total quantity
            quantity -= data;

            // Format and return the formatted output
            return Number(data.toFixed(3));
        },
    },
    {
        title: 'Quantity',
        data: null,
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            return Number.parseFloat(quantity.toFixed(3));
        },
    },
];

// Extract the ID from the current URL
const id = (new URL(window.location.href).pathname.match(/\d+/) || [])[0];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbHistory', '/api/products/' + id + '/history', columns);
