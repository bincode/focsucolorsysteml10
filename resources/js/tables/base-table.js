// Import DataTables plugin and some other modules
import '~/admin-lte/plugins/datatables/jquery.dataTables.min.js';
import '~/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js';
import '~/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js';
import '~/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js';

// Import modules to create edit and delete buttons, as well as module to show alerts
import {
    createEditButton as getEditButtonHTML,
    createDeleteButton as getTrashButtonHTML,
    createRackButton as getRackButtonHTML,
    createHistoryButton as getHistoryButtonHTML,
} from '../buttons/buttons';

import * as dialog from '../alerts/alert-utils';

// Function to create a base DataTable with specified columns and AJAX URL
function createBaseTable(tableId, url, columns, paramAjax) {

    // Initialize the DataTable with specified options
    const table = $('#' + tableId).DataTable({
        isFilterDate: false,
        ordering: false,
        paging: false,
        dom: 't',
        ajax: {
            url: url,
            data: paramAjax,
            type: 'GET',
            dataSrc: 'data'
        },
        columns: columns
    });

    // Redraw the DataTable on 'change' event of the search input
    $('.dt-search').on('change', function () {
        $.fn.dataTable.tables({ visible: true, api: true }).draw(false);
    });

    // Reload the DataTable on 'change' event of the search-reload input and trigger the change event
    $('.search-reload').on('keyup', function () {
        $.fn.dataTable.tables({ visible: true, api: true }).ajax.reload();
    }).trigger('keyup');

    // Handle click event on edit button in table
    $('#' + tableId + ' tbody').on('click', '#btn_edit', function (e) {

        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Open Edit Page with ID of selected row in URL
        location.href = window.location.pathname + '/' + data['id'] + '/edit';
    });

    // Handle click event on delete button in table
    $('#' + tableId + ' tbody').on('click', '#btn_trash', function (e) {
        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Show confirmation dialog before deleting
        dialog.showDeleteConfirmation().then(function (result) {
            if (result.value) {
                // If user confirms deletion, send DELETE request to server
                handleDeleteRequest(data['id'], url, table);
            }
        });
    });

    // Function to send DELETE request to server and handle response
    function handleDeleteRequest(id, uri, dataTable) {
        const url = uri + '/' + id;
        console.log(url);

        // Set CSRF token header for AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Send DELETE request to server
        $.ajax({
            url: url,
            type: 'DELETE',
            success: function (response, textStatus, xhr) {
                // Handle successful response from server
                if (response.status == 701) {
                    dialog.showDeleteFailedAlert();
                } else {
                    dialog.showDeleteSuccessAlert();
                    dataTable.ajax.reload();
                }
            },
            error: function (xhr, status, error) {
                // Handle error response from server
                if (xhr.status != 405) {
                    dialog.showServerErrorAlert();
                }
            }
        });
    }

    // Function to render Edit and Delete buttons with specified permissions
    function renderEditAndDeleteButtons(canEditOrDelete, canSeeHistory, canSeeRacks) {
        const historyButtonHTML = canSeeHistory ? getHistoryButtonHTML(canSeeHistory) : '';
        const rackButtonHTML = canSeeRacks ? getRackButtonHTML(canSeeRacks) : '';
        const editButtonHTML = getEditButtonHTML(canEditOrDelete);
        const trashButtonHTML = getTrashButtonHTML(canEditOrDelete);

        return historyButtonHTML + rackButtonHTML + editButtonHTML + trashButtonHTML;
    }

    $(function () {
        var $search = $('input[name="input-search"]');

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var text = $search.val();
                var result = true;

                // Filter By Search
                result =  !text || data[0].includes(text);

                return result;
            }
        );
    });

    // Return the DataTable and render function for Edit and Delete buttons
    return {
        table: table,
        renderEditAndDeleteButtons: renderEditAndDeleteButtons
    };
}

// Export the createBaseTable function for use in other modules
export {
    createBaseTable
};
