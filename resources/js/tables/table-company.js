import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
    title: 'Company Name',
    data: 'name',
    width: '15%',
}, {
    title: 'Address',
    data: 'address'
}, {
    title: 'Type',
    data: 'type',
    class: 'text-nowrap align-middle text-center',
    render: function (data, type, row) {
        return renderTypeBadge(data);
    }
}, {
    title: 'Actions',
    data: null,
    width: '5%',
    class: 'text-center',
    searchable: false,
    render: function (data, type, row) {
        // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
        const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true);

        // Return the HTML for the Edit and Delete buttons
        return editAndDeleteButtonsHTML;
    }
}];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbCompany', '/api/companies', columns);

// Method to render the type badge
function renderTypeBadge(data) {
    let badgeSupplier = '<small class="badge badge-warning"> Supplier </small>';
    let badgeCustomer = '<small class="badge badge-primary"> Customer </small>';

    switch (data) {
        case 'supplier':
            return badgeSupplier;
        case 'customer':
            return badgeCustomer;
        case 'both':
            return badgeSupplier + '&nbsp&nbsp' + badgeCustomer;
        default:
            return '<i>???</i>';
    }
}
