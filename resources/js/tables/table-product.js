import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
        title: 'Product Name',
        data: 'name',
        width: '25%'
    },
    {
        title: 'Description',
        data: 'description',
    }, {
        title: 'Stock',
        data: 'stock',
        width: '15%',
        class: 'text-center',
    },
    {
        title: 'Actions',
        data: null,
        width: '5%',
        class: 'text-center',
        searchable: false,
        render: function (data, type, row) {
            // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
            const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true, true);

            // Return the HTML for the Edit and Delete buttons
            return editAndDeleteButtonsHTML;
        }
    }
];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbProduct', '/api/products', columns);

$('.table tbody').on('click', '#btn_history', function (e) {
    // Get the row index and data from the table
    const index = $(this).parents('tr');
    const data = myTable.table.row(index).data();

    // Open Edit Page
    location.href = window.location.href + '/' + data['id'] + '/history';
});
