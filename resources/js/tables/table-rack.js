import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
    title: 'Code',
    data: 'code',
    width: '15%',
}, {
    title: 'Note',
    data: 'note'
}, {
    title: 'Capacity',
    width: '15%',
    class: 'text-right',
    data: null,
    render: function (data, type, row) {
        return data.stock + '/' + data.capacity;
    }
}, {
    title: 'Actions',
    data: null,
    width: '5%',
    class: 'text-center',
    searchable: false,
    render: function (data, type, row) {
        // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
        const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true);

        // Return the HTML for the Edit and Delete buttons
        return editAndDeleteButtonsHTML;
    }
}];

const paramAjax = {
    warehouse: $('#warehouse_id').val()
};

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbRack', '/api/racks', columns, paramAjax);
