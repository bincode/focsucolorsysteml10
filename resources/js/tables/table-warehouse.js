import { createBaseTable } from './base-table';

// Define the column configurations for the DataTable
const columns = [
    {
        title: 'Warehouse Name',
        data: 'name',
        width: '25%'
    },
    {
        title: 'Address',
        data: 'address',
    },
    {
        title: 'Capacity',
        width: '15%',
        class: 'text-right',
        data: null,
        render: function (data, type, row) {
            return data.stock + '/' + data.capacity;
        }
    },
    {
        title: 'Actions',
        data: null,
        width: '5%',
        class: 'text-center',
        searchable: false,
        render: function (data, type, row) {
            // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
            const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true, false, true);

            // Return the HTML for the Edit and Delete buttons
            return editAndDeleteButtonsHTML;
        }
    }
];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbWarehouse', '/api/warehouses', columns);

// Handle click event on edit button in table
$('#tbWarehouse tbody').on('click', '#btn_rack', function (e) {
    // Get the row index and data from the table
    const index = $(this).parents('tr');
    const data = myTable.table.row(index).data();

    // Open Edit Page with ID of selected row in URL
    location.href = window.location.pathname + '/' + data['id'] + '/racks';
});
