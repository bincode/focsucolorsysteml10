import {
    createBaseTable
} from './base-table';

// Define the column configurations for the DataTable
const columns = [{
        title: 'Date',
        data: 'date',
    },
    {
        title: 'No. Lot',
        data: 'lot',
        class: 'text-left',
    },
    {
        title: 'End Product',
        data: 'product',
        class: 'text-left',
    },
    {
        title: 'Quantity',
        data: 'quantity',
        class: 'text-left',
    },
    {
        title: 'Status',
        data: 'status',
        class: 'text-left',
    },
    {
        title: 'Actions',
        data: null,
        width: '5%',
        class: 'text-center',
        searchable: false,
        render: function (data, type, row) {
            // Call renderEditAndDeleteButtons to get the HTML for the Edit and Delete buttons
            const editAndDeleteButtonsHTML = myTable.renderEditAndDeleteButtons(true);

            // Return the HTML for the Edit and Delete buttons
            return editAndDeleteButtonsHTML;
        }
    }
];

// Call createBaseTable to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = createBaseTable('tbPlan', '/api/plans', columns);
