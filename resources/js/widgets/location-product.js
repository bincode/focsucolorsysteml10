$(function () {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Extract the ID from the current URL
    const id = (new URL(window.location.href).pathname.match(/\d+/) || [])[0];

    // Construct the API endpoint URL
    const apiUrl = `${domain}/api/racks?product=${id}`;

    // Use AJAX to load data
    $.ajax({
        url: apiUrl,
        method: 'GET',
    })
    .then(function(response) {
        let dataBody = $('#data-location');

        $.each(response.data, function(index, value) {
            let row = $('<tr>');

            let dataRack = $('<td class="p-1">').text(value.code);
            row.append(dataRack);

            let dataStock = $('<td class="p-1 text-right">').text(value.stock);
            row.append(dataStock);

            dataBody.append(row);
        });
    })
});
