import * as dialog from './alert-utils';

$('.btn-upload').on('click', function (e) {
    dialog.showFileUploadDialog(window.location.pathname + '/import');
});
