import Swal from 'sweetalert2';

// Displays a confirmation dialog for deleting an item
export const showDeleteConfirmation = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, delete it!'
    });

// Displays an alert for a failed delete operation, with an error message from the server response
export const showDeleteFailedAlert = (response) =>
    Swal.fire({
        title: "Delete Failed",
        text: response.message,
        icon: 'warning',
    });

// Displays a success alert after a successful delete operation
export const showDeleteSuccessAlert = () =>
    Swal.fire({
        title: 'Deleted!',
        text: 'Your file has been deleted.',
        icon: 'success'
    });

// Displays an alert for a server error
export const showServerErrorAlert = () =>
    Swal.fire({
        title: "Server Error",
        text: "Please contact your administrator.",
        icon: 'question',
    });

// Displays a file input dialog for uploading an image
export const showFileUploadDialog = async (url) => {
    const {
        value: file
    } = await Swal.fire({
        title: 'Choose file..',
        input: 'file',
        inputAttributes: {
            'accept': 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'aria-label': 'Upload your master data file'
        }
    });

    if (file) {
        Swal.fire({
            title: 'Uploading...',
            showConfirmButton: false,
            allowOutsideClick: false,
        });

        const reader = new FileReader();
        reader.onload = (e) => {
            const formData = new FormData();
            formData.append('file', file);

            // Get the CSRF token from the meta tag
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            console.log(url);
            fetch(url, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'X-CSRF-Token': csrfToken
                    }
                })
                .then(response => {
                    if (response.ok) {
                        const currentUrl = window.location.href.replace(/#$/, '');
                        Swal.fire({
                            title: 'Success',
                            text: 'File uploaded successfully',
                            icon: 'success'
                        });
                    } else {
                        throw new Error(response.error);
                    }
                })
                .catch(error => {
                    Swal.fire({
                        title: 'Error',
                        text: `There was a problem with the upload: ${error.message}`,
                        icon: 'error'
                    });
                    console.error('There was a problem with the fetch operation:', error);
                });
        };
        reader.readAsDataURL(file);
    }
};
