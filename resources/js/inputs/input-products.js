import initialize from '../table-inputs/select2-table-initialization.js';
import { productsSelectConfig, racksSelectConfig } from '../selects/select2-initialization.js';

// Call the initialize function with the necessary arguments
initialize('table-input-products', 'products[]', 'racks[]', productsSelectConfig, racksSelectConfig);
