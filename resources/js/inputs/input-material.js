import initialize from '../table-inputs/select2-table-initialization.js';
import { materialsSelectConfig, racksSelectConfig } from '../selects/select2-initialization.js';

// Call the initialize function with the necessary arguments
initialize('table-input-material', 'materials[]', 'racks[]', materialsSelectConfig, racksSelectConfig);
