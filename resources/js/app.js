import './bootstrap';
import $ from 'jquery';

// Set jQuery as the global variable
window.$ = window.jQuery = $;

// Event listener for keypress
$(document).on('keypress', function (e) {
    // Check if the pressed key is ArrowDown
    if (e.key === "ArrowDown") {
        // Find the next input element with class "input-focus" and trigger focus
        $(".input-focus:focus").parent().parent().next().find('.input-focus').trigger('focus');
    }
    // Check if the pressed key is ArrowUp
    if (e.key === "ArrowUp") {
        // Find the previous input element with class "input-focus" and trigger focus
        $(".input-focus:focus").parent().parent().prev().find('.input-focus').trigger('focus');
    }
    // Check if the pressed key is Enter
    if (e.key === "Enter") {
        // Trigger blur on the currently focused input element with class "input-focus"
        $(".input-focus:focus").trigger('blur');
        // Prevent the default behavior and return false
        e.preventDefault();
        return false;
    }
});
