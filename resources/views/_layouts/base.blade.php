<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('title')</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Load CSS yang di-extend oleh child view -->
    @yield('css')
    <!-- Load CSS yang di-push oleh child view -->
    @stack('css')

    <!-- Load main CSS -->
    @vite(['resources/css/app.scss'])

    <!-- Style untuk menghilangkan tombol spinner pada input number -->
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
    <div class="wrapper">

        <!-- Include navbar -->
        @include('_layouts.navbar')
        <!-- Include sidebar -->
        @include('_layouts.sidebar')

        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- Title Page -->
                            <h1>@yield('header-content')</h1>
                        </div>
                        <div class="col-sm-6">
                            <!-- Breadcrumb -->
                            <ol class="breadcrumb float-sm-right">
                                @yield('breadcumb')
                            </ol>
                        </div>
                    </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Main Content Page -->
                @yield('content')
            </section>
            <!-- /.content -->
        </div>

        <!-- Include footer -->
        @include('_layouts.footer')

        <!-- Control sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>

        <!-- Load main JS -->
        @vite(['resources/js/app.js'])

        <!-- Load JS yang di-extend oleh child view -->
        @yield('js')
        <!-- Load JS yang di-push oleh child view -->
        @stack('js')
    </div>
</body>

</html>
