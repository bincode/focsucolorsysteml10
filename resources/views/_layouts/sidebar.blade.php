<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand logo -->
    <a href="#" class="brand-link">
        <img src="#" alt="" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">FOCSystem</span>
    </a>

    <div class="sidebar">
        <!-- User panel -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src='https://ui-avatars.com/api/?name={{ Auth::user()->username }}&background=0D8ABC&color=fff'
                    class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->username }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Sidebar menu items -->
                <x-sidebar-menu label='Home' :url="route('home')" icon='fas fa-home' />

                <li class="nav-header"></li>
                <x-sidebar-menu label='Product' :url="route('products.index')" icon='fas fa-boxes' />

                <li class="nav-header">LABORATORY</li>
                <x-sidebar-menu label='Formula' :url="route('formulas.index')" icon='fas fa-flask' />

                <li class="nav-header">PPIC</li>
                <x-sidebar-menu label='Production Plan' :url="route('plans.index')" icon='fas fa-clipboard-list' />

                <li class="nav-header">DISTRIBUTION CENTER</li>
                <x-sidebar-menu label='Receive Item' :url="route('receives.index')" icon='fas fa-truck-loading' permission='receive_view'/>
                <x-sidebar-menu label='Delivery Product' :url="route('deliveries.index')" icon='fas fa-truck' permission='delivery_view'/>

                <li class="nav-header">WAREHOUSE</li>
                <x-sidebar-menu label='Release Material' :url="route('releases.index')" icon='fas fa-clipboard-check' />

                <li class="nav-header">MASTER DATA</li>
                <x-sidebar-menu label='Relation Company' :url="route('companies.index')" icon='fas fa-building' permission='master_company_view'/>
                <x-sidebar-menu label='Warehouse' :url="route('warehouses.index')" icon='fas fa-warehouse' permission='master_warehouse_view' />

                @can('admin_view')<li class="nav-header">ADMINISTRATION</li>@endcan
                <x-sidebar-menu label='Users Management' :url="route('users.index')" icon='fas fa-users' permission='admin_view' />
                <x-sidebar-menu label='Roles Management' :url="route('roles.index')" icon='fas fa-user-tag' permission='admin_view' />

                <li class="nav-header">PROFILE</li>
                <x-sidebar-menu label='Logout' :url="route('logout')" icon='fas fa-sign-out-alt' />
            </ul>
        </nav>
    </div>
</aside>
