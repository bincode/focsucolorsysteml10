@extends('_layouts.base')

@section('title', 'Home')

@section('css')
    @vite(['resources/css/app.scss'])
@endsection

@section('content')
    <div class="title m-b-md">
        Hello, {{ Auth::user()->username }}
    </div>
@endsection
