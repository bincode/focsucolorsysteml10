@extends('_layouts.base')

@section('title', 'Warehouse Data Master')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">

            <h2> WAREHOUSES </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new warehouse button -->
                <x-button-create :url="route('warehouses.create')" />

                <!-- Export warehouse list button -->
                <x-button-download :url="route('warehouses.export')" />

                <!-- Import warehouse list button -->
                <x-button-upload />
            </div>

            <!-- Warehouse table -->
            <x-card body-class='p-0'>
                <x-table id='tbWarehouse' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <!-- Warehouse index page JS -->
    @vite(['resources/js/tables/table-warehouse.js', 'resources/js/alerts/import.js'])
@endsection
