@extends('_layouts.base')

@section('title', 'Warehouses')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">
            <x-form title=Warehouse routePrefix=warehouses :param=$warehouse??null>

                <div class="row">
                    <div class="col-12">
                        {{-- Warehouse Name input field --}}
                        <x-input name='name' label='Name' :bind='$component->model' />

                        {{-- Warehouse Address input field --}}
                        <x-textarea name='address' label='Location Address' placeholder="Address Line" rows="3" :bind='$component->model' />
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection
