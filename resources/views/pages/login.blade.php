<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page Title -->
    <title>FOCSystem | Log in</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Load Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">

    @vite(['resources/css/app.scss'])
</head>

<body class="hold-transition login-page">
    <div class="login-box">

        <!-- Display Logo -->
        <div class="login-logo">
            <a href="#"><b>FOCS</b>ystem</a>
        </div>

        <!-- Display Login Form -->
        <div class="card">
            <div class="card-body login-card-body">

                <!-- Display Login Title -->
                <h3 class="login-box-msg">Sign in</h3>

                <!-- Display Login Form -->
                <form action='{{ route('attemptLogin') }}' method='POST'>
                    @csrf

                    <!-- Display Input Field for Username -->
                    <x-input name='username' type='text' placeholder='username' icon='fas fa-user' />

                    <!-- Display Input Field for Password -->
                    <x-input name='password' type='password' placeholder='password' icon='fas fa-lock' />

                    <!-- Display Login Button -->
                    <div class="row">
                        <div class="col-8"></div>
                        <div class="col-4">
                            <button name='submit' type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Load Main JavaScript -->
        <script src="{{ asset('js/adminlte.min.js') }}"></script>
</body>

</html>
