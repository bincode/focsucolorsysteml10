@extends('_layouts.base')

@section('title', 'Roles Management')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <x-form title=Role routePrefix=roles :param=$role??null>

                <div class="row">
                    <div class="col-12">
                        {{-- Role Name input field --}}
                        <x-input name='name' label='1. Role Name' :bind='$component->model' />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                         {{-- Permissions input field --}}
                         <x-checkbox-permission name='permissions[]' label='2. Give Access To' :options=$permissions :bind='$component->model' />
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection
