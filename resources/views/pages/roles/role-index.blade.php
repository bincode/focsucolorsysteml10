@extends('_layouts.base')

@section('title', 'Roles Management')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> ROLE MANAGEMENT </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new role button -->
                <x-button-create :url="route('roles.create')" />
            </div>

            <!-- Role table -->
            <x-card body-class='p-0'>
                <x-table id='tbRole' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <!-- Role index page JS -->
    @vite(['resources/js/tables/table-role.js'])
@endsection
