@extends('_layouts.base')

@section('title', 'Production Planning')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">
            <x-form id='work-order' title='Production Planning' routePrefix='plans' :param='$plan ?? null'>
                <div class="row">
                    <div class="col-4">
                        <!-- Due Date Input -->
                        <x-input daterangepicker name='date' label="Due Date" :bind='$component->model' />
                        <!-- Lot Number Input -->
                        <x-input name='lot' label='No. Lot' :bind='$component->model' />
                        {{-- Product Select --}}
                        <x-select-product name='goods' label='Product' :bind='$component->model' />
                        {{-- Fromula Input --}}
                        <x-select-formula-version name='version' label="Version" :bind='$component->model' />
                        <!-- Quantity Plan Input -->
                        <x-input name='quantity' label='Quantity Plan' :bind='$component->model' />

                        <div class="text-right">
                            <a id="create-formula" class="btn btn-sm btn-success">Create</a>
                        </div>
                    </div>
                    <div class="col-8">
                        <div id="fragmentContainer">
                            <!-- This container will hold the dynamically loaded fragments -->
                        </div>
                    </div>
                </div>


                {{-- Save button --}}
                <div class="text-right">
                    <button id="submitworkorder" type="submit" class="btn btn-primary">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/selects/select2-initialization.js', 'resources/js/daterangepicker/daterangepicker-initialization.js', 'resources/js/inputs/input-products.js', 'resources/js/fragments/work-order-fragment.js'])
@endsection
