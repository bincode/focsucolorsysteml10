@extends('_layouts.base')

@section('title', 'Users Management')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> USER MANAGEMENT </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new user button -->
                <x-button-create :url="route('users.create')" />
            </div>

            <!-- User table -->
            <x-card body-class='p-0'>
                <x-table id='tbUser' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <!-- User index page JS -->
    @vite(['resources/js/tables/table-user.js'])
@endsection
