@extends('_layouts.base')

@section('title', 'Users Management')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <x-form title=User routePrefix=users :param=$user??null>

                <div class="row">
                    <div class="col-6">
                        {{-- Username input field --}}
                        <x-input name='username' label='Username' :bind='$component->model' />

                        {{-- Surname input field --}}
                        <x-input name='surname' label='Surname' :bind='$component->model' />

                        {{-- User role select field --}}
                        <x-select-role name='role' label='Role' :bind='$component->model' />
                    </div>
                    <div class="col-6">
                        {{-- Password input field --}}
                        <x-input type='password' name='password' label='Password' placeholder='******' />

                        {{-- Password confirmation input field --}}
                        <x-input type='password' name='password_confirmation' label='Password Confirmation' placeholder='******' />
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/selects/select2-initialization.js'])
@endsection
