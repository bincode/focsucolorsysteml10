@extends('_layouts.base')

@section('title', 'Create Rack')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">

            <x-form title='Rack' routePrefix='warehouses.racks' :param='isset($rack) ? [$warehouse, $rack] : [$warehouse]'>

                <x-input name='code' type='text' label='Code*' :bind='$component->model' />
                <x-input name='name' label='On Warehouse' :bind='$warehouse' disabled />
                <x-input name='note' type='text' label='Note' placeholder='Describe what is the use of this rack' :bind='$component->model' />

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection
