@extends('_layouts.base')
@section('title', 'Racks')

@section('css')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    {{-- Main content --}}
    <div class="row d-flex justify-content-center">
        <div class="col-8">

            {{-- Header --}}
            <h2> Racks on <b>'{{ $warehouse->name }} Warehouse'</b></h2>

            {{-- Hidden input for warehouse ID --}}
            <input type="hidden" id="warehouse_id" value="{{ $warehouse->id }}">

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">

                {{-- Create button --}}
                <div class="col-4">
                    <x-button-create :url="route('warehouses.racks.create', ['warehouse' => $warehouse->id])" />
                </div>

                {{-- Search input --}}
                <div class="col-4">
                    <x-input name='search' class='form-control form-control-sm' />
                </div>

            </div>
            {{-- ./Tools --}}

            {{-- Table component --}}
            <x-card body-class='p-0'>
                <x-table id='tbRack' />
            </x-card>
        </div>
    </div>
    {{-- ./Main content --}}
@endsection

@section('js')
    <!-- Warehouse Rack index page JS -->
    @vite(['resources/js/tables/table-rack.js'])
@endsection
