@extends('_layouts.base')

@section('title', 'Formula Product')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-10">

            <h2> FORMULA PRODUCT </h2>

            <div class="row d-flex justify-content-between">
                <div class="col-8 pl-2 mb-3">
                    <!-- Create new Formula Items button -->
                    <x-button-create :url="route('formulas.create')" />
                </div>

                <div class="col-4">
                    <x-input name='input-search' class='form-control-sm search-reload' />
                </div>
            </div>

            <x-card body-class='p-0'>
                <x-table id='tbFormula' />
            </x-card>
        </div>
    </div>

@endsection

@section('js')
    <!-- Receive Item index page JS -->
    @vite(['resources/js/tables/table-formula.js'])
@endsection
