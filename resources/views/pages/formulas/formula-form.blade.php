@extends('_layouts.base')

@section('title', 'Formula')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <x-form title='Formula' routePrefix='formulas' :param='$formula ?? null'>

                <div class="row">
                    <div class="col-12">
                        <div class="row mt-3 mr-1">
                            <div class="col-6">
                                {{-- Supplier Select --}}
                                <x-select-product name='goods' label='Product' :bind='$component->model' />
                            </div>

                            <div class="col-6">
                                {{-- Reference Number Input --}}
                                <x-input name='version' label="Version" :bind='$component->model' />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table-formula-list name='table-input-products' :bind='$component->model' productTag='products'/>
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/selects/select2-initialization.js', 'resources/js/inputs/input-products.js'])
@endsection
