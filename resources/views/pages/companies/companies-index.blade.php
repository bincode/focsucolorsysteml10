@extends('_layouts.base')

@section('title', 'Companies Master Management')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> Companies Master Management </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new company button -->
                <x-button-create :url="route('companies.create')" />

                <!-- Export company list button -->
                <x-button-download :url="route('companies.export')" />

                <!-- Import company list button -->
                <x-button-upload />
            </div>

            <!-- Company table -->
            <x-card body-class='p-0'>
                <x-table id='tbCompany' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <!-- User index page JS -->
    @vite(['resources/js/tables/table-company.js', 'resources/js/alerts/import.js'])
@endsection
