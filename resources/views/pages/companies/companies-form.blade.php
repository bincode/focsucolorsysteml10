@extends('_layouts.base')

@section('title', 'Companies Master Management')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">
            <x-form title='Company' routePrefix='companies' :param='$company ?? null'>

                <div class="row">
                    <div class="col-12">
                        {{-- Username input field --}}
                        <x-input name='name' label='Company Name' :bind='$component->model' />

                        {{-- Username input field --}}
                        <x-textarea name='address' label='Address' placeholder="(Optional)" :bind='$component->model' />

                        {{-- Username input field --}}
                        <x-select-company-tag name='companytag' label="Company As ..." :bind='$component->model' />
                    </div>
                </div>


                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection
