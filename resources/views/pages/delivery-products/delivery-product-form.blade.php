@extends('_layouts.base')

@section('title', 'Delivery Product')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <x-form title='Delivery Product' routePrefix='deliveries' :param='$delivery ?? null'>

                <div class="row">
                    <div class="col-12">
                        <div class="row mt-3 mr-1">
                            <div class="col-6">
                                {{-- Delivery Date Input --}}
                                <x-input daterangepicker name='date' label="Delivery Date" :bind='$component->model' />
                            </div>

                            <div class="col-6">
                                {{-- Reference Number Input --}}
                                <x-input name='reference' label='No. Delivery Product' :bind='$component->model' />
                            </div>
                        </div>

                        {{-- Supplier Select --}}
                        <x-select-company name='customer' label='Customer' :bind='$component->model' />
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table-product-list name='table-input-goods' :bind='$component->model' productTag='goods'/>
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/selects/select2-initialization.js', 'resources/js/daterangepicker/daterangepicker-initialization.js', 'resources/js/inputs/input-goods.js'])
@endsection
