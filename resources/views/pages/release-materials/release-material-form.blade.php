@extends('_layouts.base')

@section('title', 'Release Material')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-10">
            <x-form id='release-material' title='Release Material' routePrefix='releases' :param='$release ?? null'>
                <div class="row">
                    <div class="col-3">
                        <!-- Date Input -->
                        <x-input daterangepicker name='date' label="Due Date" :bind='$component->model' />
                        <!-- Lot Number Input -->
                        <x-select-production-plan name='plan' label='No. Lot' :bind='$component->model' :disabled='isset($release)'/>
                        {{-- Product Select --}}
                        <x-input name='productName' label='Product Name' :bind='$component->model' disabled/>
                        {{-- Product Select --}}
                        <x-input name='version' label='Formula Version' :bind='$component->model' disabled/>
                        <!-- Quantity Plan Input -->
                        <x-input name='quantity_plan' label='Quantity Plan' :bind='$component->model' disabled/>
                    </div>
                    <div class="col-9">
                        <div id="fragmentContainer">
                            <!-- This container will hold the dynamically loaded fragments -->
                        </div>
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button id="submitworkorder" type="submit" class="btn btn-primary">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/selects/select2-initialization.js', 'resources/js/daterangepicker/daterangepicker-initialization.js', 'resources/js/inputs/input-materials.js', 'resources/js/fragments/release-material-fragment.js'])
@endsection
