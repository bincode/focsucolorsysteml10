@extends('_layouts.base')

@section('title', 'Data Product')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">
            <x-form title='Product' routePrefix='products' :param='$product ?? null'>

                <div class="row">
                    <div class="col-12">
                        {{-- Product Name input field --}}
                        <x-input name='name' label='Product Name' :bind='$component->model' />

                        {{-- Product Description input field --}}
                        <x-input name='description' label='Description' :bind='$component->model' />

                        {{-- Product Type select field --}}
                        <x-select-product-tag name='product_tag' label='Product Type' :bind='$component->model' />
                    </div>
                </div>

                {{-- Save button --}}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection
