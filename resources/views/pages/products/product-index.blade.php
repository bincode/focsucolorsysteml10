@extends('_layouts.base')

@section('title', 'Product List')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-10">

            <h2> PRODUCT </h2>

            <div class="row d-flex justify-content-between">
                <div class="col-8 pl-2 mb-3">
                    <!-- Create new product button -->
                    <x-button-create :url="route('products.create')" />

                    <!-- Export product list button -->
                    <x-button-download :url="route('products.export')" />

                    <!-- Import product list button -->
                    <x-button-upload />
                </div>

                <div class="col-4">
                    <x-input name='input-search' class='form-control-sm search-reload' />
                </div>
            </div>

            <!-- Product table -->
            <x-card body-class='p-0'>
                <x-table id='tbProduct' />
            </x-card>

        </div>
    </div>
@endsection

@section('js')
    <!-- User index page JS -->
    @vite(['resources/js/tables/table-product.js', 'resources/js/alerts/import.js'])
@endsection
