@extends('_layouts.base')

@section('title', 'History')

@section('content')
    <div class="row">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card>
                        <x-input daterangepicker name='tableSrcDateStart' label='Date From' class='dt-search form-control-sm' />
                        <x-input daterangepicker name='tableSrcDateEnd' label='Date To' class='dt-search form-control-sm' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <x-card>
                        <span><b>Location</b></span>
                        <x-widgets.product-location />
                    </x-card>
                </div>
            </div>
        </div>

        <div class="col-10">
            <h2><b>{{ $product->name }}</b> <span>( {{ $product->description }} )</span></h2>

            {{-- Table --}}
            <x-card body-class='p-0'>
                <x-table id='tbHistory' />
            </x-card>
            {{-- ./Table --}}
        </div>
    </div>
@endsection

@section('js')
    <!-- User Form page JS -->
    @vite(['resources/js/daterangepicker/daterangepicker-initialization.js', 'resources/js/tables/table-history.js', 'resources/js/widgets/location-product.js'])
@endsection
