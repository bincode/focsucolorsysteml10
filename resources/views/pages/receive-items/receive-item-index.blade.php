@extends('_layouts.base')

@section('title', 'Receive Item')

@section('css')
    <!-- CSRF token for AJAX requests -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card>
                        <x-input daterangepicker name='tableSrcDateStart' label='Date From' class='dt-search form-control-sm' />
                        <x-input daterangepicker name='tableSrcDateEnd' label='Date To' class='dt-search form-control-sm' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>

        <div class="col-10">

            <h2> RECEIVE ITEM </h2>

            <div class="row d-flex justify-content-between">
                <div class="col-8 pl-2 mb-3">
                    <!-- Create new Receive Items button -->
                    <x-button-create :url="route('receives.create')" />
                </div>

                <div class="col-4">
                    <x-input name='input-search' class='form-control-sm search-reload' />
                </div>
            </div>

            <x-card body-class='p-0'>
                <x-table id='tbReceive' />
            </x-card>
        </div>
    </div>

@endsection

@section('js')
    <!-- Receive Item index page JS -->
    @vite(['resources/js/tables/table-receive-item.js'])
@endsection
