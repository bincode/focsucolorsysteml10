<!-- Start of Card Container -->
<div class="{{ $cardClass }}">

    <!-- Start of Card Header -->
    @isset($cardTitle)
        <div class="card-header">

            <!-- Card Title -->
            <h3 class="card-title">
                <b>{!! $cardTitle !!}</b>
            </h3>
            <!-- End of Card Title -->

            <!-- Card Tools -->
            @if($hasCardTools())
                <div class="card-tools">

                    <!-- Collapsible Button -->
                    @if($cardCollapsible === 'collapsed')
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    @elseif(isset($cardCollapsible))
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    @endif
                    <!-- End of Collapsible Button -->

                </div>
            @endif
            <!-- End of Card Tools -->

        </div>
    @endisset
    <!-- End of Card Header -->

    <!-- Start of Card Body -->
    <div class="{{ $cardBodyClass }}">

        <!-- Dynamic Component -->
        @isset($dynamicComponent)
            <x-dynamic-component :component="$dynamicComponent" />
        @endisset
        <!-- End of Dynamic Component -->

        {{ $slot }}

    </div>
    <!-- End of Card Body -->

</div>
<!-- End of Card Container -->
