{{-- Title Forms --}}
<h2 class="mb-3"> {{ $getTitle() }} </h2>

<form {{ $attributes->merge(['action' => $getRoute(), 'method' => 'POST']) }}>
    @method($getMethod())

    {{-- Token --}}
    @csrf

    <x-card>
        {{ $slot }}
    </x-card>

</form>
