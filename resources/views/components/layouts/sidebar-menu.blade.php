<li class="nav-item has-treeview">
    <!-- Menu link -->
    <a href="{{ $url }}" class="nav-link {{ $isActive() }}">
        <!-- Menu icon -->
        <i class="nav-icon {{ $icon }}"></i>
        <!-- Menu label -->
        <p> {{ $label }} </p>
    </a>
    <!-- Submenu -->
    {{ $slot }}
</li>
