<a href="{{ $url }}" class="{{ $class }}">

    <!-- If an icon is provided, display it -->
    @if ($icon)
        <i class="mr-1 {{ $icon }}"></i>
    @endif

    <!-- Display the label -->
    {{ $label }}
</a>
