@extends('components.forms.base-input-group-component')

@section('input-item')
    <!-- Input Field -->
    <input name="{{ $name }}"
        {{ $attributes->class(['custom-control-input', 'is-invalid' => $errors->has($name), 'datepicker' => isset($daterangepicker)])->merge(['type' => 'checkbox']) }} value="{{ $getValue() }}" />
    <!-- ./Input Field -->
@overwrite
