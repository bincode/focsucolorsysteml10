@extends('components.forms.base-input-group-component')

@section('input-item')
    @foreach ($options as $option)
        <div class='form-check'>

            <!-- Input Field -->
            <input type='checkbox' name='{{ $name }}' value='{{ $option->name }}' id='{{ $option->id }}' @checked($isSelected($option->name))>
            <label for='{{ $option->name }}'>
                {{ ucwords(str_replace('_', ' ', $option->name)) }}
            </label>
        </div>
    @endforeach
@overwrite
