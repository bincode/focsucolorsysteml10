<!-- This code generates a table with the given columns and applies a few classes and styles to it. -->
<table {{ $attributes->class(['table table-hover text-nowrap datatable'])->merge(['style' => 'width:100%; margin-bottom:0']) }}>
    <thead>
        <tr>
            <!-- Loops through the columns and generates a table header cell for each one -->
            @foreach ($columns as $column)
                <th>{{ $column }}</th>
            @endforeach
        </tr>
    </thead>
</table>
