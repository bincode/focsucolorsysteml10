<table id={{ $name }} class="table table-hover datatable mt-2" style="width:100%">
    <thead class="thead-light">
        <tr>
            <th>Title</th>
            <th>Location</th>
            <th width='15%'>Quantity</th>
            <th width='15%'>Stock</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($products)
            @foreach ($products as $product)
                <!-- Render input product list component with data row -->
                <x-input-product-list :data-row='$product' :tag='$productTag'/>
            @endforeach
        @endisset

        <!-- Render input product list component without data row -->
        <x-input-product-list :tag='$productTag'/>
    </tbody>
</table>
