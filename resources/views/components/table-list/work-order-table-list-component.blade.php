<table id={{ $name }} class="table table-hover datatable mt-3" style="width:100%">
    <thead class="thead">
        <tr>
            <th class='text-center'>Materials</th>
            <th class='text-center' width='15%'>Percentage</th>
            <th class='text-center' width='20%'>Quantity</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($products)
            @foreach ($products as $product)
                <!-- Render input product list component with data row -->
                <x-input-work-list :data='$product' />
            @endforeach
        @endisset

        <!-- Render input product list component without data row -->
        <x-input-work-list />
    </tbody>
</table>
