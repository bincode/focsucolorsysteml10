<tr class="pb-2">
    <!-- Product Select -->
    <td class="pb-2">
        <x-select-product class='input-focus' fgroup-class='m-0' :name='$tag."[]"' :bind='$dataRow'/>
    </td>
    <!-- Rack Select -->
    <td class="pb-2">
        <x-select-rack class='input-focus' fgroup-class='m-0' name='racks[]' :label='null' :bind='$dataRow'/>
    </td>
    <!-- Quantity Input -->
    <td class="pb-2">
        <x-input class='input-focus' fgroup-class='m-0' name='quantity[]' :bind='$dataRow'/>
    </td>
    <!-- Stock Input (Disabled) -->
    <td class="pb-2">
        <x-input class='input-focus' fgroup-class='m-0' name='stock' disabled :bind='$dataRow'/>
    </td>
    <!-- Delete Button -->
    <td class="pb-2">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }}' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
