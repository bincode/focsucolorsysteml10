<tr class="pb-2">
    <!-- Product Select -->
    <td class="pb-2">
        <x-select-product class='input-focus' fgroup-class='m-0' name='products[]' :bind='$dataRow'/>
    </td>
    <!-- Quantity Input -->
    <td class="pb-2">
        <x-input class='input-focus' fgroup-class='m-0' name='percentage[]' :bind='$dataRow'/>
    </td>
    <!-- Delete Button -->
    <td class="pb-2">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }}' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
