<tr class="pb-2">
    <!-- Product Select -->
    <td class="pb-2">
        <x-select-product class='input-focus' fgroup-class='m-0' name='productByPlan[]' :bind='$dataRow'/>
    </td>
    <!-- Plan Quantity Input -->
    <td class="pb-2">
        <x-input class='input-focus text-center' fgroup-class='m-0' name='plan_quantity[]' :bind='$dataRow' disabled/>
    </td>
    <!-- Quantity Input -->
    <td class="pb-2">
        <x-input class='input-focus text-center' fgroup-class='m-0' name='quantity[]' :bind='$dataRow'/>
    </td>
    <!-- Existing Rack Select -->
    <td class="pb-2">
        <x-select-rack class='input-focus' fgroup-class='m-0' name='existingRack[]' :label='null' :bind='$dataRow'/>
    </td>
    <!-- Delete Button -->
    <td class="pb-2">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }}' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
