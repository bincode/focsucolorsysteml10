<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_release_materials', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('production_plan_id');
            $table->timestamps();

            $table->foreign('production_plan_id')->references('id')->on('production_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_release_materials');
    }
};
