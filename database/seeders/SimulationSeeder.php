<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Formula;
use App\Models\Product;
use App\Models\ProductionPlan;
use App\Models\Rack;
use App\Models\Transaction\InitialStock;
use App\Models\Transaction\ReceiveItem;
use App\Models\Transaction\ReleaseMaterial;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SimulationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a warehouse
        Warehouse::factory()->create();

        // Create supplier companies
        Company::factory()->supplier()->count(5)->create();

        // Create customer companies
        Company::factory()->customer()->count(5)->create();

        // Create formulas and associate them with a product
        Formula::factory()->count(5)->create(["product_id" => Product::factory()->goods()])->each(function ($formula) {

            // Seed stock for the new product
            $this->seedStockNewProduct($formula->product_id);

            // Generate a random count
            $count = fake()->numberBetween(1, 8);

            // Get a collection of materials based on the count
            $materials = $this->getMaterial($count);
            $materialCount = count($materials);

            $totalPercentage = 100;
            foreach ($materials as $index => $product) {

                // If it's not the last material, determine the percentage randomly
                $maxPercentage = floor($totalPercentage / ($materialCount - $index));
                $percentage = ($index === count($materials) - 1) ? $totalPercentage : fake()->numberBetween(1, $maxPercentage);

                // Attach the material product to the formula with the calculated percentage
                $formula->products()->attach($product->id, [
                    'percentage' => $percentage
                ]);

                // Reduce the remaining total percentage by the assigned percentage
                $totalPercentage -= $percentage;
            }
        });

        // Set the start date as the first day of the current month
        $startDate = Carbon::now()->startOfMonth();

        // Set the end date as the last day of the month two months ahead
        $endDate = Carbon::now()->addMonths(2)->endOfMonth();

        // Set the current date as the start date
        $currentDate = $startDate->addDay();

        // Loop while the current date is less than or equal to the end date
        while ($currentDate->lte($endDate)) {

            if ($currentDate->lte(Carbon::now()->addDays(2))) {

                // Create a production planning for the current date
                $productionPlanning = ProductionPlan::factory()->create(['date' => $currentDate->format("d/m/Y")]);

                // Get the planning quantity from the production planning
                $planningQuantity = $productionPlanning->quantity;

                // Load the formula and its related products with history
                $formula = $productionPlanning->formula->load('products.history');

                if ($currentDate->isPast()) {
                    foreach ($formula->products as $product) {
                        // Calculate the required quantity based on the planning quantity and product percentage
                        $needQuantity = $planningQuantity * $product->percentage / 100;

                        if ($product->stock < $needQuantity) {
                            // Calculate the order quantity as the nearest multiple of 50
                            $orderQuantity = ceil(($needQuantity - $product->stock) / 100) * 100;

                            // Place an order for the material
                            $this->orderMaterial($currentDate, $product->id, $orderQuantity);
                        }
                    }

                    $productionPlanning->update(['status' => 'Collect Material . . .']);

                    $releaseMaterial = ReleaseMaterial::factory()->create([
                        'date' => $currentDate->format("d/m/Y"),
                        'production_plan_id' => $productionPlanning->id
                    ]);

                    foreach ($formula->products as $product) {
                        // Calculate the required quantity based on the planning quantity and product percentage
                        $needQuantity = $planningQuantity * $product->percentage / 100;

                        foreach ($this->getExistingRack($product->id) as $rack) {
                            // Determine the quantity to release, taking the minimum of stock and need quantity
                            $releaseQuantity = min($rack->stock, $needQuantity);

                            // Attach the product to the release material with the appropriate details
                            $releaseMaterial->products()->attach($product->id, [
                                'rack_id' => $this->getAvailableRack($product->id),
                                'quantity' => $releaseQuantity,
                                'account_type' => "CREDIT",
                            ]);

                            // Update the remaining need quantity
                            $needQuantity -= $releaseQuantity;

                            // Break the inner loop if the need quantity is fulfilled
                            if ($needQuantity <= 0) {
                                $productionPlanning->update(['status' => 'Production Phase']);
                                break;
                            }
                        }
                    }
                }
            }
            $currentDate = $currentDate->addDay();
        }
    }

    private function orderMaterial($currentDate, $productId, $orderQuantity)
    {
        ReceiveItem::factory()->create(['date' => $currentDate->format("d/m/Y")])->products()->attach($productId, [
            'rack_id' => $this->getAvailableRack($productId),
            'quantity' => $orderQuantity,
            'account_type' => "DEBIT",
        ]);
    }

    /**
     * Seed stock for a new product.
     */
    private function seedStockNewProduct($productId)
    {
        // Create an initial stock
        InitialStock::factory()->create()->products()->attach($productId, [
            'rack_id' => $this->getAvailableRack($productId),
            'quantity' => fake()->numberBetween(1, 25) * 10,
            'account_type' => "DEBIT",
        ]);
    }

    /**
     * Get the materials based on the desired limit.
     */
    private function getMaterial($limit)
    {
        // Create a new product using the factory
        $this->seedStockNewProduct(Product::factory()->create()->id);

        // Retrieve random materials with the 'RM' tag
        $materials = Product::withTag('RM')->inRandomOrder()->limit($limit)->get();

        // Check if the retrieved materials count is less than the desired limit
        if ($materials->count() < $limit) {
            // Create additional material products using the factory until the desired limit is reached
            return Product::factory()->material()->count($limit)->create()->each(function ($product) {
                $this->seedStockNewProduct($product->id);
            });
        }

        // Return the retrieved materials
        return $materials;
    }

    /**
     * Get an available rack for a given product.
     */
    private function getAvailableRack($productId)
    {
        // Retrieve existing racks filtered by product and with product history
        $existingRack = $this->getExistingRack($productId);

        // Check if no available rack is found
        if ($existingRack->isEmpty()) {
            // Create new rack
            return Rack::factory()->create()->id;
        }

        // Return the ID of an existing available rack
        return $existingRack->random()->id;
    }

    private function getExistingRack($productId)
    {
        return Rack::filterByProduct($productId)
            ->withFilteredHistory($productId)
            ->get();
    }
}
