<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Get the list of roles, modules, and permissions from a configuration file or a database table
        $roles = ['Administrator'];
        $modules = ['Admin', 'Receive', 'Delivery'];
        $permissions = ['master_warehouse_view', 'master_company_view'];

        // Create the roles and assign them the appropriate permissions
        foreach ($roles as $roleName) {
            $role = $this->createRoleWithName($roleName);

            foreach ($permissions as $permissionName) {
                $permission = $this->createPermissionWithName($permissionName);
                $role->givePermissionTo($permission);
            }
        }

        // Create the permissions for each module and assign them to the super admin role
        $superAdmin = Role::find(1);

        foreach ($modules as $moduleName) {
            $modulePermissions = $this->getModulePermissions($moduleName);

            foreach ($modulePermissions as $permissionName) {
                $permission = $this->createPermissionWithName($permissionName);
                $superAdmin->givePermissionTo($permission);
            }
        }

        // Create the administrator user and assign them the super admin role
        $admin = User::factory()->create([
            'username' => 'administrator',
            'surname' => 'Administrator'
        ]);

        $admin->assignRole($superAdmin);
    }

    /**
     * Create a permission with the given name.
     *
     * @param string $name
     * @return \Spatie\Permission\Models\Permission
     */
    public function createPermissionWithName($name)
    {
        return Permission::create(['name' => $name, 'guard_name' => 'web']);
    }

    /**
     * Create a role with the given name.
     *
     * @param string $name
     * @return \Spatie\Permission\Models\Role
     */
    public function createRoleWithName($name)
    {
        return Role::create(['name' => $name, 'guard_name' => 'web']);
    }

    /**
     * Get the permissions for the given module.
     *
     * @param string $moduleName
     * @return array
     */
    public function getModulePermissions($moduleName)
    {
        $lcModuleName = lcfirst($moduleName);

        return [
            $lcModuleName . '_view',
            $lcModuleName . '_create',
            $lcModuleName . '_update',
            $lcModuleName . '_delete',
        ];
    }
}
