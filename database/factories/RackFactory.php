<?php

namespace Database\Factories;

use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rack>
 */
class RackFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // Random Warehouse
        $warehouse = Warehouse::inRandomOrder()->first();

        return [
            'code' => $this->faker->unique()->bothify('?###'),
            'note' => $this->faker->sentence(),
            'capacity' => 2000,
            'warehouse_id' => $warehouse->id
        ];
    }
}
