<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->company,
            'address' => $this->faker->address,
            'companytag' => $this->faker->randomElement(['supplier', 'customer', 'both']),
        ];
    }

    public function supplier(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'companytag' => 'supplier',
            ];
        });
    }

    public function customer(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'companytag' => 'customer',
            ];
        });
    }
}
