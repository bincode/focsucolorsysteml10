<?php

namespace Database\Factories\Transaction;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ReceiveItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $company_id = Company::where('companytag', 'supplier')->orWhere('companytag', 'both')->select('id')->get()->random()->id;

        return [
            'date' => fake()->dateTimeThisMonth('+3 months')->format("d/m/Y"),
            'reference' => fake()->unique()->bothify('RI-#####'),
            'company_id' => $company_id
        ];
    }
}
