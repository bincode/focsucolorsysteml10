<?php

namespace Database\Factories\Transaction;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction\InitialStock>
 */
class InitialStockFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'date' => Carbon::now()->startOfMonth()->format("d/m/Y")
        ];
    }
}
