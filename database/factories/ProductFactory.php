<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // Random Tag
        $tag = fake()->randomElement(['RM', 'WIP', 'FG']);

        return [
            'name'               => fake()->unique()->bothify($tag . ' #### ???'),
            'description'        => fake()->sentence,
            'product_tag'        => $tag,
        ];
    }

    public function goods(): Factory
    {
        return $this->state(function (array $attributes) {
            $tag = 'FG';
            return [
                'name'               => fake()->unique()->bothify($tag . ' #### ???'),
                'product_tag'        => $tag,
            ];
        });
    }

    public function material(): Factory
    {
        return $this->state(function (array $attributes) {
            $tag = 'RM';
            return [
                'name'               => fake()->unique()->bothify($tag . ' #### ???'),
                'product_tag'        => $tag,
            ];
        });
    }
}
