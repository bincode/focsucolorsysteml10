<?php

namespace Database\Factories;

use App\Models\Formula;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductionPlan>
 */
class ProductionPlanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $formula = Formula::inRandomOrder()->first();

        return [
            'date' => fake()->dateTimeThisMonth('+3 months')->format("d/m/Y"),
            'formula_id' => $formula->id,
            'lot' => fake()->unique()->bothify('P-#########'),
            'quantity' => fake()->numberBetween(1, 80) * 25,
            'status' => 'Planning Phase'
        ];
    }
}
