<?php

namespace App\Models;

use App\Traits\HistoryTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    use HasFactory, HistoryTrait;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Cast the "code" attribute.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function code(): Attribute
    {
        // Define a custom attribute cast for the "code" attribute,
        // which capitalizes the value using the ucwords function
        return Attribute::make(
            get: fn ($value) => ucwords($value),
        );
    }

    /**
     * Scope a query to include filtered history.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int|null  $productID
     * @return void
     */
    public function scopeFilterByProduct($query, $productID = null)
    {
        $query->whereHas('history', function ($historyQuery) use ($productID) {
            $historyQuery->where('product_id', $productID);
        });
    }

    /**
     * Scope a query to include filtered history.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int|null  $productID
     * @return void
     */
    public function scopeWithFilteredHistory($query, $productID = null)
    {
        $query->with(['history' => function ($historyQuery) use ($productID) {

            // Conditionally filter the history by product ID if it's provided
            $historyQuery->when(!is_null($productID), function ($q) use ($productID) {
                $q->where('product_id', $productID);
            });
        }]);
    }

    /**
     * Get the warehouse that owns the rack.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        // Define a belongsTo relationship with the Warehouse model
        return $this->belongsTo(Warehouse::class);
    }
}
