<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

class Transaction extends MorphPivot
{
    public const DEBIT_TYPES    = 'DEBIT';
    public const CREDIT_TYPES   = 'CREDIT';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_history';

    /**
     * Get the parent transactionable model.
     */
    public function transactionable()
    {
        return $this->morphTo('transactionable')->orderBy('date');
    }

    /**
     * Get the rack associated with the transaction.
     */
    public function rack()
    {
        return $this->belongsTo(Rack::class, 'rack_id');
    }

    /**
     * Get the rack associated with the transaction.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
