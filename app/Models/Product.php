<?php

namespace App\Models;

use App\Models\Transaction\InitialStock;
use App\Traits\HistoryTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, HistoryTrait;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the name attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => strtoupper($value),
        );
    }

    /**
     * Get the quantity attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function quantity(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->pivot->quantity,
        );
    }

    /**
     * Get the percentage attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function percentage(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->pivot->percentage,
        );
    }

    /**
     * Scope a query to include products with the given tag.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $tag
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithTag($query, $tag)
    {
        return $query->where('product_tag', $tag)->orWhere('product_tag', 'WIP');
    }

    /**
     * Scope a query to products that existed before a specific date.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $date
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExistedBeforeDate($query, $date)
    {
        return $query->whereHas('initialStock', function ($query) use ($date) {
            $query->where('date', '<', $date);
        });
    }

    /**
     * Scope a query to products that have a matching production plan.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int  $planId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasMatchingProductionPlan($query, $planId)
    {
        return $query->whereHas('formulas', function ($formulaQuery) use ($planId) {
            $formulaQuery->whereHas('productionPlans', function ($planQuery) use ($planId) {
                $planQuery->where('id', $planId);
            });
        });
    }

    /**
     * Get the initial stock entries assigned to this product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function initialStock()
    {
        return $this->morphedByMany(InitialStock::class, 'transactionable', 'product_history')->withPivot(['quantity', 'rack_id']);
    }

    /**
     * Get the rack associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rack()
    {
        return $this->belongsTo(Rack::class);
    }

    /**
     * Get the rack associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function racks()
    {
        return $this->belongsToMany(Rack::class, 'product_history', 'product_id', 'rack_id')->withPivot(['quantity']);
    }

    /**
     * Get the formulas associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function formulas()
    {
        return $this->belongsToMany(Formula::class)->withPivot('percentage');
    }

    /**
     * Get the production plans associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function productionPlans()
    {
        return $this->hasManyThrough(ProductionPlan::class, Formula::class);
    }
}
