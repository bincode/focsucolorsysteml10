<?php

namespace App\Models;

use App\Helpers\ProductionPlanHelper;
use App\Models\Transaction\ReleaseMaterial;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductionPlan extends Model
{
    use HasFactory;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Set the product transaction "date" attribute.
     *
     * @return Attribute
     */
    public function date(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::createFromFormat('Y-m-d', $value)->format("d/m/Y"),
            set: fn ($value) => ($value instanceof Carbon) ? $value->format('Y-m-d') : Carbon::createFromFormat('d/m/Y', $value)->format("Y-m-d")
        );
    }

    /**
     * Get the associated product for the production plan.
     *
     * @return Attribute
     */
    public function product(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->formula->product
        );
    }

    /**
     * Get the associated release material for the production plan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function release()
    {
        return $this->hasOne(ReleaseMaterial::class, 'production_plan_id');
    }

    /**
     * Get the associated formula for the production plan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formula()
    {
        return $this->belongsTo(Formula::class);
    }

    /**
     * Get the products associated with the production plan.
     *
     * @return \Illuminate\Support\Collection
     */
    /**
     * Get the products associated with the production plan.
     *
     * @return \Illuminate\Support\Collection
     */
    public function products()
    {
        return ProductionPlanHelper::getPlanMaterials($this);
    }
}
