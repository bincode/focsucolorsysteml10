<?php

namespace App\Models\Transaction;

use App\Models\Transaction;
use App\Traits\TransactionTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InitialStock extends Model
{
    use HasFactory, TransactionTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaction_intiate_stock';

    /**
     * Interact with the user's password.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function accountType(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Transaction::DEBIT_TYPES,
        );
    }

    /**
     * Interact with the user's password.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function reference(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => 'Inital Stock',
        );
    }

    /**
     * Interact with the user's password.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => '-',
        );
    }
}
