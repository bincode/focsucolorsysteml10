<?php

namespace App\Models\Transaction;

use App\Models\ProductionPlan;
use App\Models\Transaction;
use App\Traits\TransactionTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReleaseMaterial extends Model
{
    use HasFactory, TransactionTrait;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaction_release_materials';

    /**
     * Get the account type attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function accountType(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Transaction::CREDIT_TYPES,
        );
    }

    /**
     * Get the reference attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function reference(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->plan->lot,
        );
    }

    /**
     * Get the productName attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function productName(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->plan->product->name,
        );
    }

    /**
     * Get the quantityPlan attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function quantityPlan(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->plan->quantity,
        );
    }

    /**
     * Get the version attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function version(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->plan->formula->version,
        );
    }

    /**
     * Get the description attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => '<-- <b>' . $this->plan->product->name . '</b> -->',
        );
    }

    /**
     * Get the production plan associated with the release material.
     */
    public function plan()
    {
        return $this->belongsTo(ProductionPlan::class, 'production_plan_id');
    }

    /**
     * Get the materials associated with the release material.
     */
    public function productByPlan()
    {
        return $this->products();
    }
}
