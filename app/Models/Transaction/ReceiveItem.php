<?php

namespace App\Models\Transaction;

use App\Models\Company;
use App\Models\Transaction;
use App\Traits\TransactionTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiveItem extends Model
{
    use HasFactory, TransactionTrait;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaction_receive_items';

    /**
     * Get the account type attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function accountType(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Transaction::DEBIT_TYPES,
        );
    }

    /**
     * Get the description attribute cast.
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function description(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => 'Receive from <b>' . $this->supplier->name . '</b>',
        );
    }

    /**
     * Get the supplier that owns the receive item.
     */
    public function supplier()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * Get the materials associated with the receive item.
     */
    public function materials()
    {
        return $this->products();
    }
}
