<?php

namespace App\Models;

use App\Traits\HistoryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory, HistoryTrait;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Define a one-to-many relationship with the `Rack` model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function racks()
    {
        return $this->hasMany(Rack::class);
    }
}
