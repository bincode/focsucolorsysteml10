<?php

namespace App\Validators;

use App\Models\Formula;
use Illuminate\Http\Request;

class FormulaValidator
{
    public function validate(Request $request, Formula $formula)
    {
        // Get the products associated with the formula
        $formulaProducts = $formula->products;

        // Filter out any empty products from the request
        $inputProducts = array_filter($request->products);

        // Filter out any empty percentages from the request
        $inputPercentages = array_filter($request->percentage);

        // Check if the counts of formula products and input products are different
        if (count($formulaProducts) !== count($inputProducts)) {
            // The counts are different
            return false;
        }

        // Combine input products and input percentages into an associative array
        $combinedArray = array_combine($inputProducts, $inputPercentages);

        // Filter out null values from the combined array
        $combinedArray = array_filter($combinedArray);

        // Iterate over the formula products
        foreach ($formulaProducts as $product) {
            $productId = $product->id;

            // Check if the product ID exists in the combined array
            if (!array_key_exists($productId, $combinedArray)) {
                return false;
            }

            // Compare the percentage
            if (floatval($combinedArray[$productId]) !== floatval($product->percentage)) {
                return false;
            }
        }

        // No differences found
        return true;
    }
}
