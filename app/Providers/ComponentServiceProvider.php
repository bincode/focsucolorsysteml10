<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\View\Components\Layouts\SidebarMenu;
use App\View\Components\Forms\InputComponent;
use App\View\Components\Tables\BaseTableComponent;
use App\View\Components\Forms\Buttons\CreateButtonComponent;
use App\View\Components\Forms\Buttons\DownloadButtonComponent;
use App\View\Components\Forms\Buttons\UploadButtonComponent;
use App\View\Components\Forms\Checkbox\BaseCheckboxComponent;
use App\View\Components\Forms\Checkbox\PermissionCheckboxComponent;
use App\View\Components\Forms\Selects\CompanySelectComponent;
use App\View\Components\Forms\Selects\CompanyTagSelectComponent;
use App\View\Components\Forms\Selects\FormulaVersionSelectComponent;
use App\View\Components\Forms\Selects\ProductionPlanSelectComponent;
use App\View\Components\Forms\Selects\ProductSelectComponent;
use App\View\Components\Forms\Selects\ProductTagSelectComponent;
use App\View\Components\Forms\Selects\RackSelectComponent;
use App\View\Components\Forms\Selects\RoleSelectComponent;
use App\View\Components\Forms\TextAreaComponent;
use App\View\Components\InputList\InputFormulationListComponent;
use App\View\Components\InputList\InputProductListComponent;
use App\View\Components\InputList\ReleaseMaterialInputListComponent;
use App\View\Components\InputList\WorkOrderInputListComponent;
use App\View\Components\Layouts\BaseCardComponent;
use App\View\Components\Layouts\BaseFormComponent;
use App\View\Components\TableList\FormulationTableListComponent;
use App\View\Components\TableList\ProductTableListComponent;
use App\View\Components\TableList\ReleaseMaterialTableListComponent;
use App\View\Components\TableList\WorkOrderTableListComponent;

class ComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('input', InputComponent::class);
        Blade::component('textarea', TextAreaComponent::class);

        // Layout Component
        Blade::component('form', BaseFormComponent::class);
        Blade::component('sidebar-menu', SidebarMenu::class);
        Blade::component('table', BaseTableComponent::class);
        Blade::component('card', BaseCardComponent::class);

        // Buttons Component
        Blade::component('button-create', CreateButtonComponent::class);
        Blade::component('button-download', DownloadButtonComponent::class);
        Blade::component('button-upload', UploadButtonComponent::class);

        // Selects Component
        Blade::component('select-rack', RackSelectComponent::class);
        Blade::component('select-role', RoleSelectComponent::class);
        Blade::component('select-company', CompanySelectComponent::class);
        Blade::component('select-company-tag', CompanyTagSelectComponent::class);
        Blade::component('select-product', ProductSelectComponent::class);
        Blade::component('select-product-tag', ProductTagSelectComponent::class);
        Blade::component('select-formula-version', FormulaVersionSelectComponent::class);
        Blade::component('select-production-plan', ProductionPlanSelectComponent::class);

        // Checkbox Component
        Blade::component('checkbox-permission', PermissionCheckboxComponent::class);
        Blade::component('checkbox', BaseCheckboxComponent::class);

        // Table Component
        Blade::component('table-product-list', ProductTableListComponent::class);
        Blade::component('table-formula-list', FormulationTableListComponent::class);
        Blade::component('table-work-list', WorkOrderTableListComponent::class);
        Blade::component('table-release-material', ReleaseMaterialTableListComponent::class);

        // Input Component
        Blade::component('input-product-list', InputProductListComponent::class);
        Blade::component('input-formula-list', InputFormulationListComponent::class);
        Blade::component('input-work-list', WorkOrderInputListComponent::class);
        Blade::component('input-release-material', ReleaseMaterialInputListComponent::class);
    }
}
