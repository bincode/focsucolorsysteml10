<?php

namespace App\Observers;

use App\Models\Transaction\DeliveryProduct;
use App\Models\Transaction\ReceiveItem;
use App\Models\Transaction\ReleaseMaterial;

class TransactionObserver
{
    public function saved($event)
    {
        // Get the materials, quantity, and racks from the request
        $products = $this->getProducts($event);
        $quantity = $this->getQuantity();
        $locations = $this->getRack($event);

        // Sync the products with the data array
        $event->products()->sync([]);

        // Iterate over the quantity array and create an associative array with product data
        for ($index = 0, $count = count($quantity) - 1; $index < $count; $index++) {
            $event->products()->attach($products[$index], [
                'quantity' => $quantity[$index],
                'rack_id' => $locations[$index],
                'account_type' => $event->account_type,
            ]);
        }

        // Update the status of the plan if the event is a ReleaseMaterial instance
        if ($event instanceof ReleaseMaterial) {
            $event->plan->update([
                'status' => 'in Process'
            ]);
        }
    }

    public function deleting($event)
    {
        // Remove all associated products
        $event->products()->sync([]);
    }

    private function getProducts($event)
    {
        // Get the appropriate products based on the event type
        if ($event instanceof ReceiveItem) {
            return request('materials');
        }

        if ($event instanceof ReleaseMaterial) {
            return request('productByPlan');
        }

        if ($event instanceof DeliveryProduct) {
            return request('goods');
        }

        return request('products');
    }

    private function getQuantity()
    {
        // Get the quantity array from the request
        return request('quantity');
    }

    private function getRack($event)
    {
        // Get the appropriate racks based on the event type
        if ($event instanceof ReleaseMaterial) {
            return request('existingRack');
        }

        return request('racks');
    }
}
