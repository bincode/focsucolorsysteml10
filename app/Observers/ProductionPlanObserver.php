<?php

namespace App\Observers;

use App\Http\Requests\FormulaRequest;
use App\Models\Formula;
use App\Models\ProductionPlan;
use App\Validators\FormulaValidator;
use Illuminate\Support\Facades\Validator;

class ProductionPlanObserver
{
    public function saved(ProductionPlan $plan)
    {
        $existingFormula = $plan->formula;

        $validator = new FormulaValidator();
        $result = $validator->validate(request(), $existingFormula);

        // Check if the count of the existing formula is different from the input
        if ($result) {
            return;
        }

        $formulaRequest = new FormulaRequest();

        $validatedData = [
            'product_id' => request('goods'),
            'version'    => request('lot'),
        ];

        $validator = Validator::make($validatedData, $formulaRequest->rules());

        if ($validator->fails()) {
            return;
        }

        // Attach the FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        $formula = Formula::updateOrCreate(['version' => $validatedData['version'], 'product_id' => $validatedData['product_id']], $validator->validated());

        // Update plan without triggering the Observer
        $plan->withoutEvents(function () use ($plan, $formula) {
            $plan->update(['formula_id' => $formula->id]);
        });
    }
}
