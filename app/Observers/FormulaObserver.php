<?php

namespace App\Observers;

class FormulaObserver
{
    /**
     * Handle the "saved" event for the formula.
     *
     * @param  mixed  $event
     * @return void
     */
    public function saved($event)
    {
        // Get the materials, quantity, and racks from the request
        $products = request('products');
        $percentage = request('percentage');

        $data = [];

        // Iterate over the quantity array and create an associative array with product data
        for ($index = 0, $count = count($percentage) - 1; $index < $count; $index++) {
            $data[$products[$index]] = [
                'percentage' => $percentage[$index],
            ];
        }

        // Sync the products with the data array
        $event->products()->sync($data);
    }

    /**
     * Handle the "deleting" event for the formula.
     *
     * @param  mixed  $event
     * @return void
     */
    public function deleting($event)
    {
        // Remove all associated products
        $event->products()->sync([]);
    }
}
