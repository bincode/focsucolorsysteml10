<?php

namespace App\Exports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductsExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    /**
     * Returns a collection of racks, including their warehouse name, code, and capacity.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Product::get();
    }

    /**
     * Returns an array of column headings for the exported Excel file.
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Product Name',
            'Description',
            'Type',
        ];
    }

    public function map($item): array
    {
        return [
            $item['name'],
            $item['description'],
            $item['product_tag'],
        ];
    }
}
