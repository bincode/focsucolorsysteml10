<?php

namespace App\Exports;

use App\Models\Rack;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WarehousesExport implements FromCollection, WithHeadings
{
    /**
     * Returns a collection of racks, including their warehouse name, code, and capacity.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Rack::with('warehouse')
            ->get(['id', 'code', 'capacity', 'warehouse_id'])
            ->map(function ($rack) {
                return [
                    'warehouse_name' => $rack->warehouse->name,
                    'code' => $rack->code,
                    'capacity' => $rack->capacity
                ];
            });
    }

    /**
     * Returns an array of column headings for the exported Excel file.
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Warehouse Name',
            'Rack Code Name',
            'Capacity',
        ];
    }
}
