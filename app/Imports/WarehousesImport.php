<?php

namespace App\Imports;

use App\Models\Rack;
use App\Models\Warehouse;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class WarehousesImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // Create or retrieve the warehouse based on the warehouse name
            $warehouse = Warehouse::firstOrCreate([
                'name' => $row['warehouse_name'],
            ]);

            // Create or retrieve the rack within the warehouse based on the rack code
            $warehouse->racks()->where('code', $row['rack_code_name'])->firstOrCreate([
                'code' => $row['rack_code_name'],
                'capacity' => $row['capacity'],
            ]);
        }
    }
}
