<?php

namespace App\View\Components\Forms\Buttons;

use Illuminate\View\Component;

class BaseButtonComponent extends Component
{
    /**
     * The URL to redirect to when the button is clicked
     *
     * @var string
     */
    public $url;

    /**
     * The icon to display on the button, if any
     *
     * @var string|null
     */
    public $icon;

    /**
     * The label to display on the button
     *
     * @var string
     */
    public $label;

    /**
     * The CSS class(es) to apply to the button
     *
     * @var string
     */
    public $class;

    /**
     * Create a new component instance.
     *
     * @param string $label The label to display on the button
     * @param string $class The CSS class(es) to apply to the button
     * @param string|null $icon The icon to display on the button, if any
     * @param string $url The URL to redirect to when the button is clicked
     * @return void
     */
    public function __construct($label = 'button', string $class = '', string $icon = null, string $url = '#')
    {
        $this->label    = $label;
        $this->icon     = $icon;
        $this->url      = $url;
        $this->class    = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.buttons.base-button-component');
    }
}
