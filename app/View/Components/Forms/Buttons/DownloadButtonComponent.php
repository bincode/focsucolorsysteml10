<?php

namespace App\View\Components\Forms\Buttons;

class DownloadButtonComponent extends BaseButtonComponent
{
    /**
     * Create a new create button instance.
     *
     * @param string $label The label to display on the button
     * @param string $class The CSS class(es) to apply to the button
     * @param string|null $icon The icon to display on the button, if any
     * @param string $url The URL to redirect to when the button is clicked
     * @return void
     */
    public function __construct($label = 'Download', string $class = 'btn btn-sm bg-primary mr-1', string $icon = 'fas fa-download', string $url = '#')
    {
        parent::__construct($label, $class, $icon, $url);
    }
}
