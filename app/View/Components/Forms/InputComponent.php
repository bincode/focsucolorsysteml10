<?php

namespace App\View\Components\Forms;

class InputComponent extends BaseInputGroupComponent
{
    /**
     * The Daterange Picker option for the input element.
     *
     * @var mixed
     */
    public $daterangepicker;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string|null $label
     * @param string|null $icon
     * @param string|null $fgroupClass
     * @param string|null $igroupClass
     * @param mixed|null $bind
     * @param mixed|null $daterangepicker
     */
    public function __construct($name, $label = null, $icon = null, $fgroupClass = null, $igroupClass = null, $bind = null, $daterangepicker = null)
    {
        // Call the parent constructor
        parent::__construct($name, $label, $icon, $fgroupClass, $igroupClass, $bind);

        // Set the daterangepicker property
        $this->setDateRangePicker($daterangepicker);
    }

    /**
     * Set up the Daterange Picker for the input element.
     *
     * @param mixed $value
     * @return void
     */
    private function setDateRangePicker($value)
    {
        $this->daterangepicker = $value;

        // If Daterange Picker is enabled, set the icon to a calendar icon
        if (isset($value)) {
            $this->icon = 'far fa-calendar-alt';
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.input-component');
    }
}
