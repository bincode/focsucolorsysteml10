<?php

namespace App\View\Components\Forms\Selects;

class CompanyTagSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        return [
            "Supplier"  => "Supplier",
            "Customer"  => "Customer",
            "Both"      => "Both"
        ];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->companytag) && $value === ucfirst($this->bind->companytag);
    }
}
