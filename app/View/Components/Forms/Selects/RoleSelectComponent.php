<?php

namespace App\View\Components\Forms\Selects;

class RoleSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name = 'role', $selected = [], $label = 'Role', $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', '', '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        if (!isset($this->bind) || !isset($this->bind->role)) {
            return [];
        }

        return [$this->bind->role->id => "{$this->bind->role->name}"];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->role) && $value === $this->bind->role->id;
    }
}
