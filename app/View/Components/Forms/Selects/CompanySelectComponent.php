<?php

namespace App\View\Components\Forms\Selects;

class CompanySelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name = 'company', $selected = [], $label = 'Company', $bind = null)
    {
        // Call the parent constructor of BaseSelectComponent
        parent::__construct($name, null, 'Selected...', $selected, $label, '', '', '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the binding and the specific property are set
        if (!isset($this->bind) || !isset($this->bind->{$this->name})) {
            return [];
        }

        // Create an array with a single option for the company
        return [$this->bind->{$this->name}->id => "{$this->bind->{$this->name}->name}"];
    }

    /**
     * Check if a value is selected
     *
     * @return bool
     */
    public function isSelected($value)
    {
        // Check if the binding and the specific property are set, and if the value matches
        return isset($this->bind->{$this->name}) && $value === $this->bind->{$this->name}->id;
    }
}
