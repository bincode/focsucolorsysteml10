<?php

namespace App\View\Components\Forms\Selects;

class ProductTagSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        return [
            "RM"  => "Raw Material",
            "WIP" => "Work In Process",
            "FG"  => "Finish Good"
        ];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->product_tag) && $value === $this->bind->product_tag;
    }
}
