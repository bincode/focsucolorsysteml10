<?php

namespace App\View\Components\Forms\Selects;

class ProductionPlanSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $fgroupClass
     * @param array $selected
     * @param string $label
     * @param mixed $bind
     */
    public function __construct(string $name = 'plan', $fgroupClass = '', $selected = [], $label = 'Version', $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', $fgroupClass, '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        if (!isset($this->bind) || !isset($this->bind->plan)) {
            return [];
        }

        return [$this->bind->plan->id => "{$this->bind->plan->lot}"];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->plan) && $value === $this->bind->plan->id;
    }
}
