<?php

namespace App\View\Components\Forms\Selects;

use App\Models\Product;

class ProductSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @param  string  $name
     * @param  string  $fgroupClass
     * @param  array  $selected
     * @param  mixed  $bind
     * @return void
     */
    public function __construct(string $name = 'products[]', $label = null, $fgroupClass = '', $selected = [], $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', $fgroupClass, '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Get the bound product
        $bind = $this->getBindProduct();

        // If a product is available, return an array with the product's ID as the key and its name as the value
        if ($bind) {
            return [$bind->id => $bind->name];
        }

        // If no product is available, return an empty array
        return [];
    }

    /**
     * Check if a value is selected.
     *
     * @param  mixed  $value
     * @return bool
     */
    public function isSelected($value)
    {
        // Get the bound product
        $bind = $this->getBindProduct();

        // Return true if a product is available and the given value matches the product's ID
        return $bind && $value === $bind->id;
    }

    /**
     * Get the bind product from the bind object.
     *
     * @return \App\Models\Product|null
     */
    private function getBindProduct()
    {
        // If the bind object is an instance of Product, return it directly
        if ($this->bind instanceof Product) {
            return $this->bind;
        }

        // If the bind object has a "product" property, return that property's value
        if (isset($this->bind->product)) {
            return $this->bind->product;
        }

        // If no bound product is found, return null
        return null;
    }
}
