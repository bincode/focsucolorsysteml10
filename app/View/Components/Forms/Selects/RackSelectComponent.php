<?php

namespace App\View\Components\Forms\Selects;

class RackSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $fgroupClass
     * @param array $selected
     * @param string $label
     * @param mixed $bind
     */
    public function __construct(string $name = 'rack', $fgroupClass = '', $selected = [], $label = 'Rack', $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', $fgroupClass, '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the model has a pivot relationship and retrieve rack information
        if ($this->bind && $rack = $this->bind->pivot?->rack) {
            return [$rack->id => $rack->code];
        }

        // Check if the model has a rack relationship and retrieve rack information
        if ($this->bind && $rack = $this->bind->rack) {
            return [$rack->id => $rack->code];
        }

        // If neither a pivot nor a rack relationship exists, return an empty option list.
        return [];
    }

    /**
     * Check if a value is selected.
     *
     * @param mixed $value
     * @return bool
     */
    public function isSelected($value): bool
    {
        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->rack->id;
        }

        // Check if the model is bound and has a rack relationship
        if ($this->bind && isset($this->bind->rack)) {
            return $value === $this->bind->rack->id;
        }

        // Return false if the model is not bound or does not have a pivot or rack relationship
        return false;
    }
}
