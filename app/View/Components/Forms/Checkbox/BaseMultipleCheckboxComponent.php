<?php

namespace App\View\Components\Forms\Checkbox;

use App\View\Components\Forms\BaseInputGroupComponent;

class BaseMultipleCheckboxComponent extends BaseInputGroupComponent
{
    /**
     * The array of options for the checkboxes.
     *
     * @var array
     */
    public $options;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(String $name, String $label, $options = [], $bind = null)
    {
        // Call the parent constructor of the BaseInputGroupComponent
        parent::__construct($name, $label, null, null, null, $bind);

        // Set the "options" property to the given array of options
        $this->options = $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        // Return the view for the component
        return view('components.forms.checkbox.base-multiple-checkbox-component');
    }
}
