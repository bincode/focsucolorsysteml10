<?php

namespace App\View\Components\Forms\Checkbox;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionCheckboxComponent extends BaseMultipleCheckboxComponent
{
    /**
     * The array of selected permissions.
     *
     * @var array
     */
    public $selected = [];

    /**
     * Create a new component instance.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  mixed  $bind
     * @return void
     */
    public function __construct(string $name, string $label, $bind = null)
    {
        // Get the list of all permissions
        $permissionList = Permission::get();

        // Call the parent constructor of BaseMultipleCheckboxComponent
        parent::__construct($name, $label, $permissionList, null, null, $bind);

        // Check if a binding model is provided
        if (isset($bind)) {
            // Retrieve the selected permissions for the given role
            $this->selected = Role::findByName($bind->name)->permissions->pluck('name')->toArray();
        }
    }


    /**
     * Check if a value is selected.
     *
     * @param  mixed  $value
     * @return bool
     */
    public function isSelected($value): bool
    {
        return in_array($value, $this->selected);
    }
}
