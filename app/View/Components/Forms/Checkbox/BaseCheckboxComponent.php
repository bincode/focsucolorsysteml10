<?php

namespace App\View\Components\Forms\Checkbox;

use Illuminate\View\Component;

class BaseCheckboxComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(String $name, String $label, $bind = null)
    {
        parent::__construct($name, $label, null, null, null, $bind);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.checkbox.base-checkbox-component');
    }
}
