<?php

namespace App\View\Components\Layouts;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\View\Component;

/**
 * Class BaseFormComponent
 * @package App\View\Components\Layouts
 *
 * A reusable component for creating and updating forms
 */
class BaseFormComponent extends Component
{
    /**
     * The title of the form
     * @var string
     */
    public string $title;

    /**
     * The keys of the route
     * @var array
     */
    public array $routeKeys;

    /**
     * The prefix of the route
     * @var string
     */
    public string $routePrefix;

    /**
     * The model to be updated, if any
     * @var object|null
     */
    public ?object $model = null;

    /**
     * The parameters for the route
     * @var array
     */
    public array $param;

    /**
     * BaseFormComponent constructor.
     * @param string $title The title of the form
     * @param string $routePrefix The prefix of the route
     * @param mixed|null $param The parameters for the route, if any
     */
    public function __construct(string $title, string $routePrefix, $param = null)
    {
        $this->title = $title;
        $this->routePrefix = $routePrefix;
        $this->param = Arr::wrap($param);

        // Split the route prefix into an array of keys
        $this->routeKeys = collect(explode('.', $routePrefix))->map(fn ($key) => Str::singular($key))->toArray();

        // The last parameter is assumed to be the model (if any)
        $this->model = Arr::last($this->param);
    }

    /**
     * Returns the title of the form with "Create" or "Update" appended, depending on whether a model is present
     * @return string
     */
    public function getTitle(): string
    {
        return $this->model ? "{$this->title} Update" : "{$this->title} Create";
    }

    /**
     * Returns the URL for the form submission, with any parameters included
     * @return string
     */
    public function getRoute(): string
    {
        $param = [];

        // Map the route keys to the corresponding parameter IDs
        $param = collect($this->param)->mapWithKeys(function ($param, $index) {
            return [$this->routeKeys[$index] => $param['id']];
        })->toArray();

        // Return the URL with the appropriate action ("store" for create, "update" for update)
        return route("{$this->routePrefix}." . ($this->model ? 'update' : 'store'), $param);
    }

    /**
     * Returns the HTTP method for the form submission (either "POST" or "PUT")
     * @return string
     */
    public function getMethod(): string
    {
        return $this->model ? 'PUT' : 'POST';
    }

    /**
     * Render method that returns the view to be rendered
     * @return \Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('components.layouts.base-form-component');
    }
}
