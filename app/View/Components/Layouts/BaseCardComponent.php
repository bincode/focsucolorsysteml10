<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class BaseCardComponent extends Component
{
    /**
     * The theme mode for the card (full or outline).
     *
     * @var string
     */
    public $themeMode;

    /**
     * The theme color for the card.
     *
     * @var string
     */
    public $theme;

    /**
     * The CSS class for the card body.
     *
     * @var string
     */
    public $bodyClass;

    /**
     * The custom component to be displayed inside the card body.
     *
     * @var mixed
     */
    public $dynamicComponent;

    /**
     * The title for the card header.
     *
     * @var string|null
     */
    public $cardTitle;

    /**
     * Whether or not the card is collapsible.
     *
     * @var string|null
     */
    public $cardCollapsible;

    public function __construct($cardTitle = null, $themeMode = 'outline', $theme = 'primary', $dynamicComponent = null, $bodyClass = null, $cardCollapsible = null)
    {
        $this->themeMode = $themeMode;
        $this->theme = $theme;
        $this->cardTitle = $cardTitle;
        $this->cardCollapsible = $cardCollapsible;
        $this->bodyClass = $bodyClass;
        $this->dynamicComponent = $dynamicComponent;
    }

    /**
     * Generate the CSS class for the card based on its properties.
     *
     * @return string The CSS class for the card.
     */
    public function cardClass()
    {
        $classes = ['card'];

        // Add theme-related classes.
        if (isset($this->theme)) {
            $base = $this->themeMode === 'full' ? 'bg' : 'card';
            $classes[] = "{$base}-{$this->theme}";

            if ($this->themeMode === 'outline') {
                $classes[] = 'card-outline';
            }
        }

        // Add collapsible-related classes.
        if ($this->cardCollapsible === 'collapsed') {
            $classes[] = 'collapsed-card';
        }

        // Return the concatenated string of classes.
        return implode(' ', $classes);
    }

    /**
     * Generate the CSS class for the card body based on its properties.
     *
     * @return string The CSS class for the card body.
     */
    public function cardBodyClass()
    {
        $classes = ['card-body'];

        if (isset($this->bodyClass)) {
            $classes[] = $this->bodyClass;
        }

        // Return the concatenated string of classes.
        return implode(' ', $classes);
    }

    /**
     * Check if the card has any tools (e.g. collapsible button).
     *
     * @return bool Whether or not the card has any tools.
     */
    public function hasCardTools()
    {
        return isset($this->cardCollapsible);
    }

    /**
     * Render the BaseCardComponent view.
     *
     * @return \Illuminate\Contracts\View\View The rendered view.
     */
    public function render()
    {
        return view('components.layouts.base-card-component');
    }
}
