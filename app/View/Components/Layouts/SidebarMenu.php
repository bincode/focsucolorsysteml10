<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class SidebarMenu extends Component
{
    /**
     * The URL to redirect to
     *
     * @var string
     */
    public string $url;

    /**
     * The icon to display for the menu item
     *
     * @var string
     */
    public string $icon;

    /**
     * The label to display for the menu item
     *
     * @var string
     */
    public string $label;

    /**
     * The permission required to view this menu item
     *
     * @var string|null
     */
    public ?string $permission;

    /**
     * The current URL, cached in the constructor
     *
     * @var string|null
     */
    private string $currentUrl;

    /**
     * Create a new component instance.
     *
     * @param string $label The label to display for the menu item
     * @param string $url The URL to redirect to
     * @param string $icon The icon to display for the menu item
     * @return void
     */
    public function __construct($label = 'Menu', string $url = '#', string $icon = 'fas fa-circle nav-icon', string $permission = null)
    {
        $this->label        = $label;
        $this->icon         = $icon;
        $this->url          = $url;
        $this->permission   = $permission;

        $this->currentUrl = url()->current();
    }

    /**
     * Get if this sidebar menu item is active
     */
    public function isActive(): bool
    {
        return $this->url === $this->currentUrl ? 'active' : '';
    }

    /**
     * Get if the current user has permission to view this menu item
     */
    public function hasPermission(): bool
    {
        return $this->permission ? auth()->user()->hasPermissionTo($this->permission) : true;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return $this->hasPermission() ? view('components.layouts.sidebar-menu') : '';
    }
}
