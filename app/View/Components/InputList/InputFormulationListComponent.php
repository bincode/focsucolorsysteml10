<?php

namespace App\View\Components\InputList;

class InputFormulationListComponent extends BaseInputListComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        parent::__construct($data);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-list.input-formulation-list-component');
    }
}
