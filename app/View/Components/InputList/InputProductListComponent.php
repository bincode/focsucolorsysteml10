<?php

namespace App\View\Components\InputList;

class InputProductListComponent extends BaseInputListComponent
{
    public $tag;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data = null, $tag)
    {
        $this->tag = $tag;
        parent::__construct($data);
    }

    /**
     * Create Class for 'Action Delete Button'
     */
    public function makeActionDeleteButtonClass()
    {
        // Initialize an array of classes
        $classes = ['btn', 'btn-sm'];

        // Add 'btn-danger' class if $dataRow is set, otherwise add 'btn-secondary' class
        $classes[] = isset($this->dataRow) ? 'btn-danger' : 'btn-secondary';

        // Join the classes array into a string separated by spaces
        return implode(' ', $classes);
    }

    /**
     * Set 'Delete Button' attribute disabled condition
     */
    public function isDeleteButtonDisabled()
    {
        // Return an empty string if $dataRow is set, otherwise return 'disabled'
        return isset($this->dataRow) ? '' : 'disabled';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-list.input-product-list-component');
    }
}
