<?php

namespace App\View\Components\InputList;

use Illuminate\View\Component;

class BaseInputListComponent extends Component
{
    public $dataRow;

    /**
     * Create a new component instance.
     *
     * @param  mixed  $dataRow
     * @return void
     */
    public function __construct($dataRow = null)
    {
        $this->dataRow  = $dataRow;
    }

    /**
     * Create Class for 'Action Delete Button'.
     *
     * @return string
     */
    public function makeActionDeleteButtonClass()
    {
        // Initialize an array of classes
        $classes = ['btn', 'btn-sm'];

        // Add 'btn-danger' class if $dataRow is set, otherwise add 'btn-secondary' class
        $classes[] = isset($this->dataRow) ? 'btn-danger' : 'btn-secondary';

        // Join the classes array into a string separated by spaces
        return implode(' ', $classes);
    }

    /**
     * Set 'Delete Button' attribute disabled condition.
     *
     * @return string
     */
    public function isDeleteButtonDisabled()
    {
        // Return an empty string if $dataRow is set, otherwise return 'disabled'
        return isset($this->dataRow) ? '' : 'disabled';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.inputs.input-product-list');
    }
}
