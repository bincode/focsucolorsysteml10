<?php

namespace App\View\Components\TableList;

/**
 * Class ProductListTable
 *
 * @package App\View\Components\Tables
 */
class ProductTableListComponent extends BaseTableListComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $bind, $productTag = '')
    {
        parent::__construct($name, $bind, $productTag);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View The rendered view.
     */
    public function render()
    {
        return view('components.table-list.product-table-list');
    }
}
