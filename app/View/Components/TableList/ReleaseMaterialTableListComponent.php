<?php

namespace App\View\Components\TableList;

class ReleaseMaterialTableListComponent extends BaseTableListComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = 'table-release-material', $bind)
    {
        parent::__construct($name, $bind, null);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.release-material-table-list-component');
    }
}
