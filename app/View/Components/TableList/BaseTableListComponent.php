<?php

namespace App\View\Components\TableList;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

/**
 * Class ProductListTable
 *
 * @package App\View\Components\Tables
 */
class BaseTableListComponent extends Component
{
    /**
     * The products to be displayed in the table.
     *
     * @var Collection
     */
    public $products;

    /**
     * The products to be displayed in the table.
     *
     */
    public $productTag;

    /**
     * The name of the table.
     *
     * @var string
     */
    public $name;

    /**
     * Create a new component instance.
     *
     * @param string $name The name of the table.
     * @param mixed $bind The binding data.
     */
    public function __construct($name, $bind, $productTag = '')
    {
        $this->name = $name;
        $this->products = $this->loadProducts($bind);
        $this->productTag = $productTag;
    }

    /**
     * Load the products associated with the binding.
     *
     * @param mixed $bind The binding data.
     * @return Collection The collection of products.
     */
    private function loadProducts($bind)
    {
        // Check if the binding is null
        if (is_null($bind)) {
            return new Collection();
        }

        // Load the 'products' relationship of the binding
        return $bind->load('products')->products;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View The rendered view.
     */
    public function render()
    {
        return view('components.tables.product-list-table');
    }
}
