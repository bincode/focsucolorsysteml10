<?php

namespace App\View\Components\TableList;

class FormulationTableListComponent extends BaseTableListComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = 'table-formula', $bind)
    {
        parent::__construct($name, $bind, null);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.formulation-table-list');
    }
}
