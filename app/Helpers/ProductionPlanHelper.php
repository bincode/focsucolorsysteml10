<?php

namespace App\Helpers;

use App\Models\ProductionPlan;
use Illuminate\Support\Collection;

class ProductionPlanHelper
{
    /**
     * Add plan quantity to the collection based on production quantity.
     *
     * @param  \Illuminate\Support\Collection  $collection
     * @param  int  $productionQuantity
     * @return \Illuminate\Support\Collection
     */
    public static function addPlanQuantity(Collection $collection, int $productionQuantity): Collection
    {
        return $collection->map(function ($item) use ($productionQuantity) {
            // Calculate the needed production quantity based on the percentage of the item in the formula
            $need = $item->pivot->percentage * $productionQuantity / 100;

            // Set the calculated production quantity as the plan production quantity for the item
            $item->plan_quantity = $need;

            return $item;
        });
    }

    /**
     * Merge the collection with the matcher collection based on production quantity.
     *
     * @param  \Illuminate\Support\Collection  $collection
     * @param  \Illuminate\Support\Collection  $matcherCollection
     * @param  int  $productionQuantity
     * @return \Illuminate\Support\Collection
     */
    public static function merge(Collection $collection, Collection $matcherCollection, int $productionQuantity): Collection
    {
        return $collection->map(function ($product) use ($matcherCollection, $productionQuantity) {
            // Find the matching product from the formula products
            $matchingProduct = $matcherCollection->where('id', $product->id)->first();

            // Get the percentage from the matching product or default to 0
            $percentage = $matchingProduct ? $matchingProduct->pivot->percentage : 0;

            // Calculate the needed production quantity based on the percentage
            $need = $percentage * $productionQuantity / 100;

            // Get the rack ID from the release product's pivot
            $rackId = $product->pivot->rack_id;

            // Update the plan quantity and rack ID for the product
            $product->plan_quantity = $need;
            $product->rack_id = $rackId;

            return $product;
        });
    }

    /**
     * Get the plan materials for the given production plan.
     *
     * @param  \App\Models\ProductionPlan  $model
     * @return \Illuminate\Support\Collection
     */
    public static function getPlanMaterials(ProductionPlan $model)
    {
        // Get the formula products associated with the production plan
        $formulaProducts = $model->formula->products;

        // Get the production quantity for the production plan
        $productionQuantity = $model->quantity;

        // Add plan quantity to the formula products
        $products = self::addPlanQuantity($formulaProducts, $productionQuantity);

        // If release products exist, merge them with the formula products
        if ($model->release) {

            // Get the release products associated with the production plan
            $releaseProducts = $model->release->products;

            $products = self::merge($releaseProducts, $products, $productionQuantity);
        }

        // Return the final collection of plan materials
        return $products;
    }
}
