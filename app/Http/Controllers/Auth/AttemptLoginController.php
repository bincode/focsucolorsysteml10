<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AttemptLoginController extends Controller
{
    /**
     * Handle the incoming authentication request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Authenticate user using provided credentials
        if (!auth()->attempt($request->only('username', 'password'))) {
            // Redirect back to login page with error message if authentication fails
            return redirect()->back()->withInput($request->only('username', 'password'))->withErrors([
                'password' => 'Wrong password'
            ]);
        }

        // Redirect to intended page or homepage upon successful authentication
        return redirect()->intended('/');
    }
}
