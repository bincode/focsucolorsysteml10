<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
     * Handle the incoming logout request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        // Log out the current user
        auth()->logout();

        // Redirect to the login route
        return redirect()->route('login');
    }
}
