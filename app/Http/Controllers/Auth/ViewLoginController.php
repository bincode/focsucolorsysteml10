<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class ViewLoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        // Check user authentication
        if (auth()->check()) {
            // Redirect to intended URL if available, otherwise redirect to homepage
            return redirect()->intended('/');
        }

        // Show login page for unauthenticated user
        return view('pages.login');
    }
}
