<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormulaRequest;
use App\Models\Formula;
use App\Observers\FormulaObserver;
use App\View\Components\TableList\WorkOrderTableListComponent;

class FormulaController extends Controller
{
    /**
     * Display the index page for formulas.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        // Render the formula-index view
        return view('pages.formulas.formula-index');
    }

    /**
     * Display the form for creating a new formula.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        // Render the formula-form view to show the form for creating a new formula
        return view('pages.formulas.formula-form');
    }

    /**
     * Store a newly created formula in storage.
     *
     * @param  \App\Http\Requests\FormulaRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormulaRequest $request)
    {
        // Attach FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        // Create a new formula in the database using the validated data from the request
        Formula::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('formulas.index')->with('success', 'Formula created successfully.');
    }

    /**
     * Display the form for editing the specified formula.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Formula $formula)
    {
        // Render the formula-form view and pass the specified formula for editing
        return view('pages.formulas.formula-form', compact('formula'));
    }

    /**
     * Update the specified formula in storage.
     *
     * @param  \App\Http\Requests\FormulaRequest  $request
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FormulaRequest $request, Formula $formula)
    {
        // Attach FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        // Update the formula in the database with the validated data from the request
        $formula->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('formulas.index')->with('success', 'Formula updated successfully.');
    }

    /**
     * Load the fragment input for the specified formula.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Contracts\View\View
     */
    public function loadFragmentInputFormula(Formula $formula)
    {
        // Create a new instance of WorkOrderTableListComponent
        $component = new WorkOrderTableListComponent("table-input-products", $formula);

        // Load the products relationship for the formula and calculate the plan quantity for each product
        $products = $formula->load('products')->products->map(function ($product) {
            $need = $product->percentage * request('quantity') / 100;
            $product->plan_quantity = $need;
            return $product;
        });

        // Render the component with the specified name and pass the products as a parameter
        $content = $component->render()->withName('table-input-products')->withProducts($products);

        // Return the rendered content
        return $content;
    }
}
