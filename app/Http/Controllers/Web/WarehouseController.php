<?php

namespace App\Http\Controllers\Web;

use App\Exports\WarehousesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\WarehouseRequest;
use App\Imports\WarehousesImport;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.warehouses.warehouse-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.warehouses.warehouse-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\WarehouseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseRequest $request)
    {
        $warehouse = Warehouse::create($request->all());

        return redirect()
            ->intended(route('warehouses.index'))
            ->with('toast_success', "Warehouse '{$warehouse->name}' created successfully.");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse)
    {
        return view('pages.warehouses.warehouse-form', compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\WarehouseRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(WarehouseRequest $request, Warehouse $warehouse)
    {
        $warehouse->update($request->all());

        return redirect()
            ->intended(route('warehouses.index'))
            ->with('toast_success', "Warehouse '{$warehouse->name}' updated successfully.");
    }

    /**
     * Downloads an Excel file containing data from the warehouses table.
     *
     * @return void
     */
    public function export()
    {
        return Excel::download(new WarehousesExport, 'Data Warehouse.xlsx');
    }

    public function import(Request $request)
    {
        Excel::import(new WarehousesImport, $request->file('file'));

        return response()->json([
            'message' => 'File uploaded successfully'
        ]);
    }
}
