<?php

namespace App\Http\Controllers\Web;

use App\Exports\CompaniesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Imports\CompaniesImport;
use App\Models\Company;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.companies.companies-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.companies.companies-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        Company::create($request->all());

        return redirect()->route('companies.index')
            ->with('toast_success', 'Created Successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('pages.companies.companies-form', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CompanyRequest  $request
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $company->update($request->all());

        return redirect()->route('companies.index')
            ->with('toast_success', 'Updated Successfully!');
    }

    /**
     * Downloads an Excel file containing data from the warehouses table.
     *
     * @return void
     */
    public function export()
    {
        return Excel::download(new CompaniesExport, 'Data Company List.xlsx');
    }

    public function import(Request $request)
    {
        Excel::import(new CompaniesImport, $request->file('file'));

        return response()->json([
            'message' => 'File uploaded successfully'
        ]);
    }
}

