<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryProductRequest;
use App\Models\Transaction\DeliveryProduct;
use App\Observers\TransactionObserver;

class DeliveryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show the index page for receive items
        return view('pages.delivery-products.delivery-product-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show the form for creating a new receive item
        return view('pages.delivery-products.delivery-product-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryProductRequest $request)
    {
        // Attach TransactionObserver to the DeliveryProduct model
        DeliveryProduct::observe(TransactionObserver::class);

        // Create a new receive item with the provided data
        DeliveryProduct::create($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('deliveries.index')->with('success', 'Delivery item created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeliveryProduct  $receive
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryProduct $delivery)
    {
        // Show the form for editing the specified receive item
        return view('pages.delivery-products.delivery-product-form', compact('delivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeliveryProduct  $receive
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryProductRequest $request, DeliveryProduct $delivery)
    {
        // Attach TransactionObserver to the DeliveryProduct model
        DeliveryProduct::observe(TransactionObserver::class);

        // Update the receive item with the provided data
        $delivery->update($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('deliveries.index')->with('success', 'Receive item updated successfully.');
    }
}
