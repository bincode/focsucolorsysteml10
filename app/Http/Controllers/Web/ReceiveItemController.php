<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReceiveItemRequest;
use App\Models\Transaction\ReceiveItem;
use App\Observers\TransactionObserver;

class ReceiveItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show the index page for receive items
        return view('pages.receive-items.receive-item-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show the form for creating a new receive item
        return view('pages.receive-items.receive-item-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceiveItemRequest $request)
    {
        // Attach TransactionObserver to the ReceiveItem model
        ReceiveItem::observe(TransactionObserver::class);

        // Create a new receive item with the provided data
        ReceiveItem::create($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('receives.index')->with('success', 'Receive item created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReceiveItem  $receive
     * @return \Illuminate\Http\Response
     */
    public function edit(ReceiveItem $receife)
    {
        // Show the form for editing the specified receive item
        return view('pages.receive-items.receive-item-form', compact('receife'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReceiveItem  $receive
     * @return \Illuminate\Http\Response
     */
    public function update(ReceiveItemRequest $request, ReceiveItem $receife)
    {
        // Attach TransactionObserver to the ReceiveItem model
        ReceiveItem::observe(TransactionObserver::class);

        // Update the receive item with the provided data
        $receife->update($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('receives.index')->with('success', 'Receive item updated successfully.');
    }
}
