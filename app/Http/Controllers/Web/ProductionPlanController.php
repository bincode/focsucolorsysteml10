<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductionPlanRequest;
use App\Models\ProductionPlan;
use App\Observers\ProductionPlanObserver;
use App\View\Components\TableList\ReleaseMaterialTableListComponent;

class ProductionPlanController extends Controller
{
    /**
     * Display the index page for plans.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        // Render the plan-index view
        return view('pages.product-plans.product-plan-index');
    }

    /**
     * Display the form for creating a new plan.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        // Render the plan-form view to show the form for creating a new plan
        return view('pages.product-plans.product-plan-form');
    }

    /**
     * Store a newly created plan in storage.
     *
     * @param  \App\Http\Requests\ProductionPlanRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductionPlanRequest $request)
    {
        // Create a new plan in the database using the validated data from the request
        ProductionPlan::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('plans.index')->with('success', 'Plan created successfully.');
    }

    /**
     * Display the form for editing the specified plan.
     *
     * @param  \App\Models\ProductionPlan  $plan
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(ProductionPlan $plan)
    {
        // Render the plan-form view and pass the specified plan for editing
        return view('pages.product-plans.product-plan-form', compact('plan'));
    }

    /**
     * Update the specified plan in storage.
     *
     * @param  \App\Http\Requests\ProductionPlanRequest  $request
     * @param  \App\Models\ProductionPlan  $plan
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductionPlanRequest $request, ProductionPlan $plan)
    {
        // Attach FormulaObserver to the Formula model
        ProductionPlan::observe(ProductionPlanObserver::class);

        // Update the plan in the database with the validated data from the request
        $plan->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('plans.index')->with('success', 'Plan updated successfully.');
    }

    /**
     * Load the fragment input for the specified production plan.
     *
     * @param  \App\Models\ProductionPlan  $plan
     * @return \Illuminate\Contracts\View\View
     */
    public function loadFragmentInputMaterial(ProductionPlan $plan)
    {
        // Create a new instance of ReleaseMaterialTableListComponent
        $component = new ReleaseMaterialTableListComponent("table-input-material", $plan->formula);

        // Get the products associated with the production plan
        $products = $plan->products();

        // Render the component with the specified name and pass the products as a parameter
        $content = $component->render()->withName('table-input-material')->withProducts($products);

        // Return the rendered content
        return $content;
    }
}
