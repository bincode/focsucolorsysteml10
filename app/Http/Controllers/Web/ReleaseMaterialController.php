<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReleaseMaterialRequest;
use App\Models\Transaction\ReleaseMaterial;
use App\Observers\TransactionObserver;
use Illuminate\Http\Request;

class ReleaseMaterialController extends Controller
{
    /**
     * Display a listing of the release materials.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show the index page for release materials
        return view('pages.release-materials.release-material-index');
    }

    /**
     * Show the form for creating a new release material.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show the form for creating a new release material
        return view('pages.release-materials.release-material-form');
    }

    /**
     * Store a newly created release material in storage.
     *
     * @param  \App\Http\Requests\ReleaseMaterialRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ReleaseMaterialRequest $request)
    {
        // Attach TransactionObserver to the ReleaseMaterial model
        ReleaseMaterial::observe(TransactionObserver::class);

        // Create a new release material in the database using the validated data from the request
        ReleaseMaterial::create($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('releases.index')->with('success', 'Release Material created successfully.');
    }

    /**
     * Show the form for editing the specified release material.
     *
     * @param  \App\Models\Transaction\ReleaseMaterial  $release
     * @return \Illuminate\Http\Response
     */
    public function edit(ReleaseMaterial $release)
    {
        // Show the form for editing the specified release material
        return view('pages.release-materials.release-material-form', compact('release'));
    }

    /**
     * Update the specified release material in storage.
     *
     * @param  \App\Http\Requests\ReleaseMaterialRequest  $request
     * @param  \App\Models\Transaction\ReleaseMaterial  $release
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ReleaseMaterialRequest $request, ReleaseMaterial $release)
    {
        // Attach TransactionObserver to the ReleaseMaterial model
        ReleaseMaterial::observe(TransactionObserver::class);

        // Update the release material in the database with the validated data from the request
        $release->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('releases.index')->with('success', 'Release Material updated successfully.');
    }
}
