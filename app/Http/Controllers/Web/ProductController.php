<?php

namespace App\Http\Controllers\Web;

use App\Exports\ProductsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Imports\ProductsImport;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Render the product-index view to display a listing of products
        return view('pages.products.product-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Render the product-form view to show the form for creating a new product
        return view('pages.products.product-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        // Create a new product in the database using the validated data from the request
        Product::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('products.index')->with('success', 'Product created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // Render the product-form view and pass the specified product for editing
        return view('pages.products.product-form', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        // Update the product in the database with the validated data from the request
        $product->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('products.index')->with('success', 'Product updated successfully.');
    }

    /**
     * Downloads an Excel file containing data from the warehouses table.
     *
     * @return void
     */
    public function export()
    {
        return Excel::download(new ProductsExport, 'Data Products.xlsx');
    }

    public function import(Request $request)
    {
        Excel::import(new ProductsImport, $request->file('file'));

        return response()->json([
            'message' => 'File uploaded successfully'
        ]);
    }
}
