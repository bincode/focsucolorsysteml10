<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\RackRequest;
use App\Models\Rack;
use App\Models\Warehouse;

class WarehouseRackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function index(Warehouse $warehouse)
    {
        return view('pages.warehouse-racks.warehouse-rack-index', compact('warehouse'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function create(Warehouse $warehouse)
    {
        return view('pages.warehouse-racks.warehouse-rack-form', compact('warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RackRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function store(RackRequest $request, Warehouse $warehouse)
    {
        $warehouse->racks()->create($request->all());

        return redirect()->intended(route('warehouses.racks.index', [
            'warehouse' => $warehouse->id
        ]))->with('toast_success', "Created Rack <b>'{$request->code}'</b>, Successfully!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @param  \App\Models\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse, Rack $rack)
    {
        return view('pages.warehouse-racks.warehouse-rack-form', compact('rack', 'warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RackRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @param  \App\Models\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function update(RackRequest $request, Warehouse $warehouse, Rack $rack)
    {
        $rack->update($request->all());

        return redirect()->intended(route('warehouses.racks.index', [
            'warehouse' => $warehouse->id
        ]))->with('toast_success', "Created Rack <b>'{$request->code}'</b>, Successfully!");
    }
}
