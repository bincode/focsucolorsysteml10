<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WarehouseResource;
use App\Models\Warehouse;

class ApiWarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a query instance
        $query = Warehouse::query();

        // Include the total capacity
        $query->withSum('racks', 'capacity');

        // Include the total capacity
        $query->with(['racks.history']);

        // Order the warehoses by name
        $query->orderBy('name');

        // Retrieve the warehoses
        $warehouses = $query->get();

        return WarehouseResource::collection($warehouses);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {
        try {
            $warehouse->racks()->delete();
            $warehouse->delete();
            return response()->json(['message' => 'Warehouse deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete warehouse'], 500);
        }
    }
}
