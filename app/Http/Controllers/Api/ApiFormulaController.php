<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormulaResource;
use App\Models\Formula;
use App\Observers\FormulaObserver;
use App\Validators\FormulaValidator;
use Illuminate\Http\Request;

class ApiFormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Create a query instance for the Formula model
        $query = Formula::query();

        // Apply product filter if present
        $query->when(request()->has('product'), function ($q) {
            $q->where('product_id', request('product'));
        });

        // Include the 'product' relationship in the query
        $query->with('product');

        // Retrieve the formulas
        $formulas = $query->get();

        // Return the collection of formulas as a resource
        return FormulaResource::collection($formulas);
    }

    public function validationFormula(Request $request, Formula $formula)
    {
        $validator = new FormulaValidator();
        $result = $validator->validate($request, $formula);

        if ($result) {
            // Formula valid
            return response()->json(['success' => true]);
        } else {
            // Formula invalid
            return response()->json(['success' => false, 'errors' => 'Perubahan Penambahan atau Pengurangan Material']);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Formula $formula)
    {
        // Attach FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        try {
            // Delete the formula
            $formula->delete();

            // Return success message if deletion is successful
            return response()->json(['message' => 'Formula deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete formula'], 500);
        }
    }
}
