<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Models\Company;

class ApiCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Create a query for the Company model
        $query = Company::query();

        // Filter by Relation Type
        $query->when(request()->has('type'), function ($q) {
            $q->where('companytag', request('type'))->orWhere('companytag', 'both');
        });

        // Order the results by name
        $query->orderBy('name');

        // Get the results
        $companies = $query->get();

        // Return the collection of companies as a resource
        return CompanyResource::collection($companies);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        try {
            // Delete the company
            $company->delete();
            return response()->json(['message' => 'Company deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete companies'], 500);
        }
    }
}
