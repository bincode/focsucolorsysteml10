<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductionPlanResource;
use App\Models\ProductionPlan;

class ApiProductionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Create a query instance for the ProductionPlan model
        $query = ProductionPlan::query();

        // Apply status filter if provided in the request
        $query->when(request()->has('status'), function ($q) {
            $q->where('status', request('status'));
        });

        // Include the 'product' relationship in the query
        $query->with('formula.product');

        // Order the results by date
        $query->orderBy('date');

        // Retrieve the plans
        $plans = $query->get();

        // Return the collection of plans as a resource
        return ProductionPlanResource::collection($plans);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductionPlan  $plan
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ProductionPlan $plan)
    {
        try {
            // Delete the plan
            $plan->delete();

            // Return success message if deletion is successful
            return response()->json(['message' => 'ProductionPlan deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete ProductionPlan'], 500);
        }
    }
}
