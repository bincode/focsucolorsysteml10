<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReceiveItemResource;
use App\Models\Transaction\ReceiveItem;

class ApiReceiveItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Create a query for the ReceiveItem model
        $query = ReceiveItem::query();

        // Eager load the 'supplier' relationship
        $query->with('supplier');

        // Order the results by date
        $query->orderBy('date');

        // Get the results
        $receiveItems = $query->get();

        // Return the collection of receive items as a resource
        return ReceiveItemResource::collection($receiveItems);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReceiveItem  $receife
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceiveItem $receife)
    {
        // Attach TransactionObserver to the ReceiveItem model
        ReceiveItem::observe(TransactionObserver::class);

        try {
            // Delete the receife
            $receife->delete();
            return response()->json(['message' => 'Receive Item deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete receifes'], 500);
        }
    }
}
