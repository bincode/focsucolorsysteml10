<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ApiProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a query instance for the Product model
        $query = Product::query();

        // Apply tag filter if present
        $query->when(request()->has('tag'), function ($product) {
            $product->withTag(request('tag'));
        });

        // Apply plan filter if present
        $query->when(request()->has('plan'), function ($product) {
            $product->hasMatchingProductionPlan(request('plan'));
        });

        // Apply search filter if present
        $query->when(request()->has('search'), function ($q) {
            $q->where('name', 'like', '%' . request('search') . '%');
        });

        // Include the history relationship
        $query->with('history');

        // Order the products by name
        $query->orderBy('name');

        // Retrieve the products
        $products = $query->get();

        // Return the collection of products as a resource
        return ProductResource::collection($products);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            // Delete the product
            $product->delete();

            // Return success message
            return response()->json(['message' => 'Product deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete product'], 500);
        }
    }
}
