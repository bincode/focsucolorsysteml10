<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RackResource;
use App\Models\Rack;

class ApiRackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get a query builder instance for the Rack model
        $query = Rack::query();

        // Apply warehouse filter if provided in the request
        $query->when(request()->has('warehouse'), function ($q) {
            $q->where('warehouse_id', request('warehouse'));
        });

        // Apply product filter if provided in the request
        $query->when(request()->has('product'), function ($q) {
            $q->filterByProduct(request('product'));
        });

        // Include the history relationship with a condition if product ID is provided
        $query->withFilteredHistory(request('product'));

        // Order the racks by rack code in alphabetical order
        $query->orderBy('code', 'asc');

        // Retrieve the racks
        $racks = $query->get();

        // Return a collection of RackResource objects created from the filtered racks
        return RackResource::collection($racks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rack $rack)
    {
        try {
            // Delete the rack
            $rack->delete();
            // Return success message if deletion is successful
            return response()->json(['message' => 'Rack deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete rack'], 500);
        }
    }
}
