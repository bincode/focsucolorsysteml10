<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryProductResource;
use App\Models\Transaction\DeliveryProduct;
use App\Observers\TransactionObserver;

class ApiDeliveryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Create a query for the DeliveryProduct model
        $query = DeliveryProduct::query();

        // Eager load the 'customer' relationship
        $query->with('customer');

        // Order the results by date
        $query->orderBy('date');

        // Get the results
        $receiveItems = $query->get();

        // Return the collection of receive items as a resource
        return DeliveryProductResource::collection($receiveItems);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeliveryProduct  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryProduct $delivery)
    {
        // Attach TransactionObserver to the DeliveryProduct model
        DeliveryProduct::observe(TransactionObserver::class);

        try {
            // Delete the delivery
            $delivery->delete();
            return response()->json(['message' => 'Receive Item deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error message if deletion fails
            return response()->json(['message' => 'Failed to delete deliveries'], 500);
        }
    }
}
