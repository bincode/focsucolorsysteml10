<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReleaseMaterialResource;
use App\Models\Transaction\ReleaseMaterial;

class ApiReleaseMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create a query for the ReleaseMaterial model
        $query = ReleaseMaterial::query();

        // Include the plan and its related formula and product
        $query->with('plan.formula.product');

        // Retrieve the release materials
        $release = $query->get();

        // Return the release materials as a collection of ReleaseMaterialResource instances
        return ReleaseMaterialResource::collection($release);
    }
}
