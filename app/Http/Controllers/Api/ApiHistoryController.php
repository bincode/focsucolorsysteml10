<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HistoryResource;
use App\Models\Product;
use Carbon\Carbon;

class ApiHistoryController extends Controller
{
    /**
     * Get History by Product
     *
     * @param Product $product
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function indexShowProduct(Product $product)
    {
        // Load the product with its history and transactionable relationships
        $product = $product->load('history.transactionable');

        // Sort the history by the transaction date
        $history = $product->history->sortBy(function ($history) {
            return Carbon::createFromFormat('d/m/Y', $history->transactionable->date);
        });

        // Return the sorted history as a resource collection
        return HistoryResource::collection($history);
    }
}
