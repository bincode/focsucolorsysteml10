<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'username'      => $this->username,
            'surname'      => $this->surname,
            'role'          => $this->roles->first()->name ?? '-',
            'attempt_clock' => $this->attempt_clock ?? '-',
            'attempt_ip'    => $this->attempt_ip ?? '-',
        ];
    }
}
