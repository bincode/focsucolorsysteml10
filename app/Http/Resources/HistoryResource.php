<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type'               => $this->transactionable_type,
            'date'               => $this->transactionable->date,
            'reference'          => $this->transactionable->reference,
            'description'        => $this->transactionable->description,
            'location'           => $this->rack->code,

            'in'            => ($this->account_type) == 'DEBIT' ? $this->quantity : 0,
            'out'           => ($this->account_type) == 'CREDIT' ? $this->quantity : 0,
        ];
    }
}
