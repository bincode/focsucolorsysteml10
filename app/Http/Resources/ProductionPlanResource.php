<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductionPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'date'          => $this->date,
            'lot'           => $this->lot,
            'product'       => $this->formula->product->name,
            'version'       => $this->formula->version,
            'quantity'      => $this->quantity,
            'status'        => $this->status
        ];
    }
}
