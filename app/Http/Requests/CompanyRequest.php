<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'name' => [
                'required',
                'unique:companies,name'
            ],
            'companytag' => [
                'required'
            ],
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the code field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['name'][1] .= ',' . $this->company->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
