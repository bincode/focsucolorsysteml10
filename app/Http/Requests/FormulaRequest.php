<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormulaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {

        // Merge the value of the 'goods' field into the 'product_id' field
        $this->merge([
            'product_id' => $this->goods,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Define the validation rules for the request
        $rules = [
            'product_id' => [
                'required',
            ],
            'version' => [
                'required',
            ],
        ];

        // Return the validation rules
        return $rules;
    }
}
