<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReleaseMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        // Merge the value of the 'plan' field into the 'production_plan_id' field
        $this->merge([
            'production_plan_id' => $this->plan,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'plan' => [
                'required',
            ],
        ];

        // Return the validation rules for the request
        return $rules;
    }
}
