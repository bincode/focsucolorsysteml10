<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductionPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        // Merge the value of the 'customer' field into the 'company_id' field
        $this->merge([
            'formula_id' => $this->version,
            'status'    => "Planning in Progress"
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'lot' => [
                'required',
                'unique:production_plans,lot'
            ],
            'quantity' => [
                'required'
            ],
            'date' => [
                'required'
            ],
            'formula_id' => [
                'required'
            ],
            'status' => [
                'required'
            ],
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the code field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['lot'][1] .= ',' . $this->plan->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
