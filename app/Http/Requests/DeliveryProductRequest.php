<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        // Merge the value of the 'customer' field into the 'company_id' field
        $this->merge([
            'company_id' => $this->customer,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'reference' => [
                'required',
                'unique:transaction_delivery_products,reference'
            ],
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the code field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['reference'][1] .= ',' . $this->delivery->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
