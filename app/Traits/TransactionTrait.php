<?php

namespace App\Traits;

use App\Models\Product;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait TransactionTrait
{
    /**
     * Set the product transaction "date".
     *
     */
    public function date(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::createFromFormat('Y-m-d', $value)->format("d/m/Y"),
            set: fn ($value) => ($value instanceof Carbon) ? $value->format('Y-m-d') : Carbon::createFromFormat('d/m/Y', $value)->format("Y-m-d")
        );
    }

    /**
     * Get the products associated with the history.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphToMany(Product::class, 'transactionable', 'product_history')->withPivot(['quantity', 'rack_id'])->using(Transaction::class);
    }
}
