<?php

namespace App\Traits;

use App\Models\Transaction;

trait HistoryTrait
{
    /**
     * Get the history of product transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Get the last stock quantity.
     *
     * @return float
     */
    public function getStockAttribute()
    {
        return $this->history->reduce(function ($stock, $transaction) {
            // Check if the transaction item is an instance of Transaction model
            if (!$transaction instanceof Transaction) {
                return 0;
            }

            // Update the stock based on the transaction type
            return $stock + ($transaction->quantity * ($transaction->account_type == Transaction::DEBIT_TYPES ? 1 : -1));
        }, 0);
    }
}
