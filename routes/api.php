<?php

use App\Http\Controllers\Api\ApiCompanyController;
use App\Http\Controllers\Api\ApiDeliveryProductController;
use App\Http\Controllers\Api\ApiFormulaController;
use App\Http\Controllers\Api\ApiHistoryController;
use App\Http\Controllers\Api\ApiProductController;
use App\Http\Controllers\Api\ApiProductionPlanController;
use App\Http\Controllers\Api\ApiRackController;
use App\Http\Controllers\Api\ApiReceiveItemController;
use App\Http\Controllers\Api\ApiReleaseMaterialController;
use App\Http\Controllers\Api\ApiRoleController;
use App\Http\Controllers\Api\ApiUserController;
use App\Http\Controllers\Api\ApiWarehouseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('users', ApiUserController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('roles', ApiRoleController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('warehouses', ApiWarehouseController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('racks', ApiRackController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('companies', ApiCompanyController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('products', ApiProductController::class)->only([
    'index', 'destroy'
]);

Route::get('/products/{product}/history', [ApiHistoryController::class, 'indexShowProduct'])->name('api.product.history');

Route::apiResource('receives', ApiReceiveItemController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('deliveries', ApiDeliveryProductController::class)->only([
    'index', 'destroy'
]);

Route::post('/formulas/{formula}/validation', [ApiFormulaController::class, 'validationFormula'])->name('api.formula.validation');
Route::put('/formulas/{formula}/validation', [ApiFormulaController::class, 'validationFormula'])->name('api.formula.validation');
Route::apiResource('formulas', ApiFormulaController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('plans', ApiProductionPlanController::class)->only([
    'index', 'destroy'
]);

Route::apiResource('releases', ApiReleaseMaterialController::class)->only([
    'index', 'destroy'
]);
