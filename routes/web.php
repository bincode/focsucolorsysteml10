<?php

use App\Http\Controllers\Auth\AttemptLoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\ViewLoginController;
use App\Http\Controllers\Web\CompanyController;
use App\Http\Controllers\Web\DeliveryProductController;
use App\Http\Controllers\Web\FormulaController;
use App\Http\Controllers\Web\HistoryController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\ProductionPlanController;
use App\Http\Controllers\Web\ReceiveItemController;
use App\Http\Controllers\Web\ReleaseMaterialController;
use App\Http\Controllers\Web\RoleController;
use App\Http\Controllers\Web\UserController;
use App\Http\Controllers\Web\WarehouseController;
use App\Http\Controllers\Web\WarehouseRackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', ViewLoginController::class)->name('login');
Route::post('login', AttemptLoginController::class)->name('attemptLogin');
Route::get('logout', LogoutController::class)->middleware('auth')->name('logout');

Route::middleware('auth')->group(function () {
    Route::view('/', 'pages.home')->name('home');
});

Route::middleware('auth')->group(function () {
    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);

    Route::get('warehouses/export', [WarehouseController::class, 'export'])->name('warehouses.export');
    Route::post('warehouses/import', [WarehouseController::class, 'import'])->name('warehouses.import');
    Route::resource('warehouses', WarehouseController::class);

    Route::resource('warehouses.racks', WarehouseRackController::class);

    Route::get('companies/export', [CompanyController::class, 'export'])->name('companies.export');
    Route::post('companies/import', [CompanyController::class, 'import'])->name('companies.import');
    Route::resource('companies', CompanyController::class);

    Route::get('products/{product}/history', [HistoryController::class, 'showProductHistory']);
    Route::get('products/export', [ProductController::class, 'export'])->name('products.export');
    Route::post('products/import', [ProductController::class, 'import'])->name('products.import');
    Route::resource('products', ProductController::class);

    Route::resource('receives', ReceiveItemController::class);
    Route::resource('deliveries', DeliveryProductController::class);

    Route::get('formulas/{formula}/input', [FormulaController::class, 'loadFragmentInputFormula']);
    Route::resource('formulas', FormulaController::class);

    Route::get('plans/{plan}/input', [ProductionPlanController::class, 'loadFragmentInputMaterial']);
    Route::resource('plans', ProductionPlanController::class);

    Route::resource('releases', ReleaseMaterialController::class);
});
